<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>
<%@ include file="include/script.jsp"%>
<script>
$(document).ready(function(){
	console.log("jquery is ready");
	$("form").submit(function (e) {
		console.log($("#startDate").val());
		if($("#startDate").val()==""){
			alert("You must select the start date of the week!");
			e.preventDefault();
		}
		else{
			alert("You have successfully submitted a new Habits Log!");
		}
	});
});
</script>
</head>
<body>


  <%@ include file="include/header.jsp"%>
  <div id="main" class="container">
    <div class="row">
      <div class="col-lg-12">
        <h1>BMI & Habits Log</h1>
      </div>
    </div>
    
    <ul class="nav nav-tabs" role="tablist">
        <li><a href="<c:url value="bhl"/>">Log Home</a></li>
        <li><a href="<c:url value="bmiLog"/>">Record BMI</a></li>
        <li class="active"><a href="#"/>Record Habits</a></li>
        <li><a href="<c:url value="logCharts"/>">Charts</a></li>
      </ul>
    
    <h3>Records your habits about eating, drinking and smoking in here every week</h3>
	<br><br>
	<form role="form" action="addHabitsLog" method="post">
		<div class="row">
		<div class="form-group">
			<label class="col-sm-2 control-label">Start Date:</label> 
			<div class="col-sm-2"><input id="startDate" class="form-control" type="date" name="startDate"/> </div>
			(Please select the Monday of the week)
		</div>
		</div>
		<br><br>
		<div class="row">
		<div class="col-sm-4">
		<h4>Eating Vegetables and Fruits</h4>
		<input type="checkbox" name="eating" value="mon">Monday<br>
		<input type="checkbox" name="eating" value="tue">Tuesday<br>
		<input type="checkbox" name="eating" value="wed">Wednesday<br>
		<input type="checkbox" name="eating" value="thu">Thursday<br>
		<input type="checkbox" name="eating" value="fri">Friday<br>
		<input type="checkbox" name="eating" value="sat">Saturday<br>
		<input type="checkbox" name="eating" value="sun">Sunday<br>
		</div>
		
		<div class="col-sm-4">
		<h4>Drinking</h4>
		<input type="checkbox" name="drinking" value="mon">Monday<br>
		<input type="checkbox" name="drinking" value="tue">Tuesday<br>
		<input type="checkbox" name="drinking" value="wed">Wednesday<br>
		<input type="checkbox" name="drinking" value="thu">Thursday<br>
		<input type="checkbox" name="drinking" value="fri">Friday<br>
		<input type="checkbox" name="drinking" value="sat">Saturday<br>
		<input type="checkbox" name="drinking" value="sun">Sunday<br>
		</div>
		

		<div class="col-sm-4">
		<h4>Smoking</h4>
		<input type="checkbox" name="smoking" value="mon">Monday<br>
		<input type="checkbox" name="smoking" value="tue">Tuesday<br>
		<input type="checkbox" name="smoking" value="wed">Wednesday<br>
		<input type="checkbox" name="smoking" value="thu">Thursday<br>
		<input type="checkbox" name="smoking" value="fri">Friday<br>
		<input type="checkbox" name="smoking" value="sat">Saturday<br>
		<input type="checkbox" name="smoking" value="sun">Sunday<br>
		</div>
		
		</div>
		<br>
		<div class="form-group">
			<button type="submit" class="btn btn-default">Submit</button>
		</div>
	</form>
    
  </div>
  <%@ include file="include/footer.jsp"%>
  

</body>
</html>