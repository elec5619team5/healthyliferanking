<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>
<%@ include file="include/script.jsp"%>
</head>
<body>

	<%@ include file="include/header.jsp"%>
	<div id="main" class="container">

		<div class="row">
			<div class="col-lg-12">
				<h1>Steam Page</h1>
			</div>
		</div>

		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="#">Steam Home</a></li>
			<li><a href="<c:url value="steamForm"/>">Record Steam</a></li>
			<li><a href="<c:url value="steamStatistics"/>">Steam Statistics</a></li>
		</ul>

		<h3>Steam Log:</h3>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Week Starts Date</th>
					<th>PlayingHour</th>
					<th>Score <span class="glyphicon glyphicon-info-sign"></span>
					</th>
					<th>Suggestions <span class="glyphicon glyphicon-info-sign"></span>
					</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${model.PlayingTime}" var="log">
					<tr>
						<td><c:out value="${log.customerName}" /></td>
						<td><c:out value="${log.playingTime}" /></td>
						<td><c:out value="${log.score}" /></td>
						<td><c:out value="${log.suggestion}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<br>

	</div>
	<%@ include file="include/footer.jsp"%>


</body>
</html>