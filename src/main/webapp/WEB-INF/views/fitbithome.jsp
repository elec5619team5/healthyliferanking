<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>
</head>
<body>


	<%@ include file="include/header.jsp"%>
	<div id="main" class="container">

		<div class="row">
			<div class="col-lg-12">
				<h1>Fitbit Home</h1>
			</div>
		</div>

		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="#">Fitbit Home</a></li>
			<li><a href="<c:url value="FitbitMarks"/>">Marks</a></li>
			<li><a href="<c:url value="Statics"/>">Statics</a></li>
		</ul>

		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-6 col-md-12">
					<h3>Personal Detail</h3>
					<table>
						<td>
						<tr>
							<td>Full Name:</td>
							<td>${userInfo.fullName}</td>
						</tr>
						<tr>
							<td>Display Name:</td>
							<td>${userInfo.displayName}</td>
						</tr>
						<tr>
							<td>Gender:</td>
							<td>${userInfo.gender}</td>
						</tr>
						<tr>
							<td>Date of Birth:</td>
							<td>${userInfo.dateOfBirth}</td>
						</tr>
						<tr>
							<td>Height:</td>
							<td>${userInfo.height}</td>
						</tr>
						<tr>
							<td>Weight:</td>
							<td>${userInfo.weight}</td>
						</tr>
						<tr>
							<td>Stride Lenght Walking:</td>
							<td>${userInfo.strideLengthWalking}</td>
						</tr>
						<tr>
							<td>Stride Lenght Running:</td>
							<td>${userInfo.strideLengthRunning}</td>
						</tr>
						<tr>
							<td>Country:</td>
							<td>${userInfo.country}</td>
						</tr>
						</td>
					</table>
					<br>
					<div class="col-lg-6 col-md-12">
						<div class="panel panel-red">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-bicycle fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge" style="font-size: 35px">
											<c:out value="${totalscore}" />
											/ 25
										</div>
										<div>Fitbit</div>
									</div>
								</div>
							</div>
							<a href="<c:url value="FitbitMarks"/>">
								<div class="panel-footer">
									<span class="pull-left">View Details</span> <span
										class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="include/footer.jsp"%>
	<%@ include file="include/script.jsp"%>
</body>
</html>