<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
    	var score_0 = ${score_0};
    	var score_2 = ${score_2};
    	var score_3 = ${score_3};
    	var score_5 = ${score_5};
    	var score_10 = ${score_10};
    	var score_15 = ${score_15};
    	var score_20 = ${score_20};
    	var score_25 = ${score_25};
    	
        var data = google.visualization.arrayToDataTable([
['Features', 'Days'],
          ['0', score_0],
          ['2',  score_2],
          ['3',  score_3],
          ['5',  score_5],
          ['10', score_10],
          ['15', score_15],
          ['20', score_20],
          ['25', score_25]
        ]);

        var options = {
          title: 'Score Bar Chart',
          vAxis: {title: 'Steam Score Diagram',  titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

        chart.draw(data, options);
      }
    </script>
</head>
<body>


	<%@ include file="include/header.jsp"%>
	<div id="main" class="container">

		<div class="row">
			<div class="col-lg-12">
				<h1>Steam Home</h1>
			</div>
		</div>
		<div class="row">
		<div class="col-lg-6">
		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="updatesteam">Steam Home</a></li>
			<li><a href="<c:url value="steamForm"/>">Record Steam</a></li>
			<li><a href="<c:url value="#"/>">Steam Statistics</a></li>
		</ul>
		</div>
		<div class="col-lg-6 col-md-12">
	<div class="col-lg-6 col-md-6">
            <div class="panel panel-dark">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-steam fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge"><c:out value="${totalscore}"/> / 25</div>
                    <div>Steam</div>
                  </div>
                </div>
              </div>
              <a href="<c:url value="/updatesteam"/>">
                <div class="panel-footer">
                  <span class="pull-left">View Details</span> <span
                    class="pull-right"><i
                    class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
          </div>
          <br><br><br><br><br>          <h3>Score Bar Chart</h3>
		<div id="chart_div" style="width: 900px; height: 500px;z-index:10"></div>
		</div>
		<%@ include file="include/footer.jsp"%>
		<%@ include file="include/script.jsp"%>
</body>
</html>