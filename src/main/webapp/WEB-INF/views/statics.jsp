<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
    	var actmark = ${activities.mark};
    	var glumark = ${glucose.mark};
    	var heamark = ${heartrate.mark};
    	var blomark = ${bloodpressure.mark};
    	var slemark = ${sleep.mark}
        var data = google.visualization.arrayToDataTable([
          ['Features', 'Marks'],
          ['activities', 	actmark],
          ['glucose',  glumark],
          ['heartrate',  heamark],
          ['bloodpressure',  blomark],
          ['sleep', slemark]
        ]);

        var options = {
          title: 'Mark Bar Chart',
          vAxis: {title: 'features',  titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

        chart.draw(data, options);
      }
    </script>
</head>
<body>


	<%@ include file="include/header.jsp"%>
	<div id="main" class="container">

		<div class="row">
			<div class="col-lg-12">
				<h1>Fitbit Home</h1>
			</div>
		</div>

		<ul class="nav nav-tabs" role="tablist">
			<li><a href="<c:url value="fitbitpersondetail"/>" />Fitbit Home</a></li>
			<li><a href="<c:url value="FitbitMarks"/>" />Marks</a></li>
			<li class="active"><a href="#">Statics</a></li>
		</ul>
	<h3>Marks Bar Chart</h3>
		<div id="chart_div" style="width: 900px; height: 500px;"></div>

		<%@ include file="include/footer.jsp"%>
		<%@ include file="include/script.jsp"%>
</body>
</html>