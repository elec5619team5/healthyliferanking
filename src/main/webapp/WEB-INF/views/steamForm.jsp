<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>
<%@ include file="include/script.jsp"%>
<script>
	function checkInput(){
		var getValue=document.getElementById("gameTime").value;
		if(getValue==""||getValue>24){
			alert("You submit an empty form or invalid hours, please enter your palying time.");
		}
		
	}

</script>
</head>
<body>


	<%@ include file="include/header.jsp"%>
	<div id="main" class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1>Steam Log</h1>
			</div>
		</div>

		<ul class="nav nav-tabs" role="tablist">
			<li><a href="<c:url value="updatesteam"/>">Steam Home</a></li>
			<li class="active"><a href="#">Steam Record</a></li>
			<li><a href="<c:url value="steamStatistics"/>">Steam Statistics</a></li>
		</ul>

		<form class="form-horizontal" role="form" action="addSteamLog"
			method="post">
			<div class="form-group">
				<label class="col-sm-2 control-label">Playing Hours:</label>
				<div class="col-sm-2">
					<input id="gameTime" class="form-control" type="number" name="playingTime" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" onclick="checkInput()" class="btn btn-default">Submit</button>
				</div>
			</div>

		</form>
	</div>
	<%@ include file="include/footer.jsp"%>
</body>
</html>