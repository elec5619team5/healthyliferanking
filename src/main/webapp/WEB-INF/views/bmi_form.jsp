<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>
<%@ include file="include/script.jsp"%>
<script>
$(document).ready(function(){
	console.log("jquery is ready");
	$("form").submit(function (e) {
		console.log($("#startDate").val());
		if($("#startDate").val()=="" || $("#height").val()=="" || $("#weight").val()==""){
			alert("Some input field is empty");
			e.preventDefault();
		}
		else{
			alert("You have successfully submitted a new BMI Log!");
		}
	});
});
</script>
</head>
<body>


  <%@ include file="include/header.jsp"%>
  <div id="main" class="container">
    <div class="row">
      <div class="col-lg-12">
        <h1>BMI & Habits Log</h1>
      </div>
    </div>
    
    <ul class="nav nav-tabs" role="tablist">
        <li><a href="<c:url value="bhl"/>">Log Home</a></li>
        <li class="active"><a href="#">Record BMI</a></li>
        <li><a href="<c:url value="habitsLog"/>">Record Habits</a></li>
        <li><a href="<c:url value="logCharts"/>">Charts</a></li>
      </ul>
    
    <h3>Records your height and weight in here every week</h3>

		<form class="form-horizontal" role="form" action="addBmiLog" method="post">
			<div class="form-group">
				<label class="col-sm-2 control-label">Start Date:</label> 
				<div class="col-sm-2"><input id="startDate" class="form-control" type="date" name="startDate"/> </div>
				(Please select the Monday of the week)
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Height (m): </label>
				<div class="col-sm-2"><input id="height" class="form-control" type="number" step="0.01" name="height" value="${lastBmiLog.height }" /> </div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Weight (kg):</label> 
				<div class="col-sm-2"><input id="weight" class="form-control" type="number" step="1" name="weight" value="${lastBmiLog.weight }" /> </div>
			</div>
			
			<!--  <input type="submit" value="Submit"/> -->
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10"><button type="submit" class="btn btn-default">Submit</button></div>
			</div>
			
		</form>

  </div>
  <%@ include file="include/footer.jsp"%>
  

</body>
</html>