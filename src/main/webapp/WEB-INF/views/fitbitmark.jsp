<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>
</head>
<body>


	<%@ include file="include/header.jsp"%>
	<div id="main" class="container">

		<div class="row">
			<div class="col-lg-12">
				<h1>Fitbit Home</h1>
			</div>
		</div>

		<ul class="nav nav-tabs" role="tablist">
			<li><a href="<c:url value="fitbitpersondetail"/>" />Fitbit Home</a></li>
			<li class="active"><a href="#" />Marks</a></li>
			<li><a href="<c:url value="Statics"/>">Statics</a></li>
		</ul>
		<h3>
			Total Score:
			<c:out value="${totalscore}" />
		</h3>
		<br>
		<div class="row">
			<div class="col-lg-6">
				<h3>Activities</h3>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Calories</th>
							<th>Steps</th>
							<th>Distance</th>
							<th>Mark</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><c:out value="${activities.calories}" /></td>
							<td><c:out value="${activities.steps}" /></td>
							<td><c:out value="${activities.distance}" /></td>
							<td><c:out value="${activities.mark}" /></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-lg-6">
				<h3>Bloodpressure</h3>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Ave Diastolic Blood Pressure(mm Hg)</th>
							<th>Mark</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><c:out value="${bloodpressure.avebloodpressure}" /></td>
							<td><c:out value="${bloodpressure.mark}" /></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-lg-6">
				<h3>Glucose</h3>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Ave Morning Glucose(mmol/l)</th>
							<th>Ave Afternoon Glucose(mmol/l)</th>
							<th>Ave Evening Glucose(mmol/l)</th>
							<th>Ave Glucose</th>
							<th>Mark</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><c:out value="${glucose.avemoringglucose}" /></td>
							<td><c:out value="${glucose.aveafternoonglucose}" /></td>
							<td><c:out value="${glucose.aveeveningglucose}" /></td>
							<td><c:out value="${glucose.aveglucose}" /></td>
							<td><c:out value="${glucose.mark}" /></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-lg-6">
				<h3>HeartRate</h3>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Resting Heart Rate(bpm)</th>
							<th>Normal Heart Rate(bpm)</th>
							<th>Ave Heart Rate(bpm)</th>
							<th>Mark</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><c:out value="${heartrate.restingheartrate}" /></td>
							<td><c:out value="${heartrate.normalheartrate}" /></td>
							<td><c:out value="${heartrate.aveheartrate}" /></td>
							<td><c:out value="${heartrate.mark}" /></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-lg-6">
				<h3>Sleep</h3>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Time To Bed</th>
							<th>Time In Bed</th>
							<th>Efficiency</th>
							<th>Mark</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><c:out value="${sleep.timetobed}" /></td>
							<td><c:out value="${sleep.timeinbed}" /></td>
							<td><c:out value="${sleep.efficiency}" /></td>
							<td><c:out value="${sleep.mark}" /></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<%@ include file="include/footer.jsp"%>
		<%@ include file="include/script.jsp"%>
</body>
</html>