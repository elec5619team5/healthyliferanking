<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>
<%@ include file="include/script.jsp"%>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	 <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawEatingChart);
      function drawEatingChart() {

    	  var sumEating = ${model.habitsCounter.sumEating};
    	  var sumNotEating = ${model.habitsCounter.sumNotEating};
    	  
        var data = google.visualization.arrayToDataTable([
          ['YesOrNo', 'Num of days'],
          ['Eating Well',     sumEating],
          ['Not Eating Well', sumNotEating]
        ]);

        var options = {
          title: 'Eating Well'
        };

        var chart = new google.visualization.PieChart(document.getElementById('eating_piechart'));

        chart.draw(data, options);
      }
      
      google.setOnLoadCallback(drawDrinkingChart);
      function drawDrinkingChart() {

    	  var sumDrinking = ${model.habitsCounter.sumDrinking};
    	  var sumNotDrinking = ${model.habitsCounter.sumNotDrinking};
    	  
        var data = google.visualization.arrayToDataTable([
          ['YesOrNo', 'Num of days'],
          ['Not Drinking',     sumNotDrinking],
          ['Drinking', sumDrinking]
        ]);

        var options = {
          title: 'Drinking Alcohol'
        };

        var chart = new google.visualization.PieChart(document.getElementById('drinking_piechart'));

        chart.draw(data, options);
      }
      
      google.setOnLoadCallback(drawSmokingChart);
      function drawSmokingChart() {

    	  var sumSmoking = ${model.habitsCounter.sumSmoking};
    	  var sumNotSmoking = ${model.habitsCounter.sumNotSmoking};
    	  
        var data = google.visualization.arrayToDataTable([
          ['YesOrNo', 'Num of days'],
          ['Not Smoking',     sumNotSmoking],
          ['Smoking', sumSmoking]
        ]);

        var options = {
          title: 'Smoking Cigarette'
        };

        var chart = new google.visualization.PieChart(document.getElementById('smoking_piechart'));

        chart.draw(data, options);
      }
    </script>
</head>
<body>

  <%@ include file="include/header.jsp"%>
  <div id="main" class="container">
  
    <div class="row">
      <div class="col-lg-12">
        <h1>Habits Charts</h1>
      </div>
    </div>
    
      <ul class="nav nav-tabs" role="tablist">
        <li><a href="<c:url value="bhl"/>">Log Home</a></li>
        <li><a href="<c:url value="bmiLog"/>">Record BMI</a></li>
        <li><a href="<c:url value="habitsLog"/>">Record Habits</a></li>
        <li class="active"><a href="<c:url value="#"/>">Charts</a></li>
      </ul>
      
       <div id="eating_piechart" style="width: 900px; height: 500px;"></div>
       <div id="drinking_piechart" style="width: 900px; height: 500px;"></div>
       <div id="smoking_piechart" style="width: 900px; height: 500px;"></div>
    
  </div>
  <%@ include file="include/footer.jsp"%>


</body>
</html>