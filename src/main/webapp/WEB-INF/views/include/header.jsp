      <nav class="navbar navbar-default" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span> <span
                class="icon-bar"></span> <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/home" /> "> Healthy Life Ranking</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse"
            id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="<c:url value="/dashboard" />">Dashboard</a></li>
			<li><a href="<c:url value="/bhl" />">BMI & Habbit Log</a></li>
			<li><a href="<c:url value="/updatesteam" />" >Steam</a></li>
              <li><a href="<c:url value="/facebook" />">Facebook</a></li>
              <li><a href="<c:url value="/fitbitApiAuth" />" >Fitbit</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <sec:authorize access="isAuthenticated()"
                var="isAuthenticated">
                <sec:authentication var="user" property="principal" />
                <li class="dropdown"><a href="#"
                  class="dropdown-toggle" data-toggle="dropdown"><span
                    class="text-uppercase">${user.username}</span><b
                    class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="<c:url value="/profile" />">PROFILE</a></li>
                    <li><a
                      href="<c:url value="/j_spring_security_logout" />">LOG
                        OUT</a></li>
                  </ul></li>
              </sec:authorize>

              <c:if test="${not isAuthenticated}">
                <li><a href="<c:url value="/login" />">LOGIN</a></li>
              </c:if>

            </ul>
          </div>
          <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
      </nav>
