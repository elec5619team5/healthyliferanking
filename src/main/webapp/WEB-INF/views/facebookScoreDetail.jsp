<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Facebook Score Detail</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<%@ include file="include/stylesheet.jsp"%>

</head>
<body>

<%@ include file="include/header.jsp"%>
	<div id="main" class="container">
	<h1>Facebook</h1>
	    <ul class="nav nav-tabs" role="tablist">
        <li><a href="<c:url value="/facebookProfile"/>">Profile</a></li>
        <li class="active"><a href="<c:url value="/facebookScore"/>">Score Details</a></li>
        <li><a href="<c:url value="/facebookHistory"/>">Score History</a></li>
      </ul>
      <c:if test="${not empty facebookData }">
	<table class="table table-striped">
			<thead>
				<tr>
					<th>Record</th> <th>Status</th> <th>Photo</th> <th>Friend</th>
				</tr>
			</thead>
			<tbody>
  		  		<tr>
    				<td>Lastest Record (${facebookData.recordDate })</td> 
    				<td>${facebookData.statusNumber}</td>
    				<td>${facebookData.photoNumber }</td>
    				<td>${facebookData.friendNumber } </td> 
    				
    	  		</tr> 
    	  		
    	  		<tr>
    	  		<c:if test="${empty  preFacebookData}">
    				<td>Previous Record (not exist)</td> 
    				<td>0</td>
    				<td>0</td>
    				<td>0</td> 
    			</c:if>
    			
    	  		<c:if test="${not empty  preFacebookData}">
    				<td>Previous Record (${preFacebookData.recordDate})</td> 
    				<td>${preFacebookData.statusNumber}</td>
    				<td>${preFacebookData.photoNumber }</td>
    				<td>${preFacebookData.friendNumber } </td> 
    			</c:if>
    	  		</tr> 
    	  		
    	  		<tr>
    				<td>Increase</td> 
    				<td>${facebookData.newStatusNumber}</td>
    				<td>${facebookData.newPhotoNumber }</td>
    				<td>${facebookData.newFriendNumber } </td> 
    				
    	  		</tr>
    	  		
    	  		<tr>
    				<td>Score</td> 
    				<td>${facebookData.statusScore}</td>
    				<td>${facebookData.photoScore }</td>
    				<td>${facebookData.friendScore } </td> 
    				
    	  		</tr>
    	  		
    	  		<tr>
    				<td>Total Score</td> 
    				<td>${facebookData.totalScore}</td>
    				<td></td>
    				<td></td>	
    	  		</tr>
    	  		 
   			 </tbody>
		</table>
		</c:if>
		<c:if test="${empty facebookData }"> 
    	<div>
    	<h3>You have not connected to Facebook</h3>
		<h3>You will get 0 score in facebook component</h3>
			<form action="<c:url value="/connect"/>"  method="POST">
				<input type="hidden" name="scope" value="publish_stream,offline_access" />
				<button type="submit" text="Connect with Facebook">Connect</button>
						
			</form>
		</div>
    
	 	</c:if>
	
	</div>
    <%@ include file="include/footer.jsp"%>
  <%@ include file="include/script.jsp"%>	
</body>
</html>