<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>
<%@ include file="include/script.jsp"%>
<script>
$(document).ready(function(){
	console.log("jquery is ready");
	$(".bmiValue").each(function(){
		var fixedNumber = parseFloat($(this).text()).toFixed(2);
		$(this).text(fixedNumber);
	});
});
</script>
</head>
<body>

  <%@ include file="include/header.jsp"%>
  <div id="main" class="container">
  
    <div class="row">
      <div class="col-lg-12">
        <h1>BMI & Habits Log</h1>
      </div>
    </div>
    
      <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#">Log Home</a></li>
        <li><a href="<c:url value="bmiLog"/>">Record BMI</a></li>
        <li><a href="<c:url value="habitsLog"/>">Record Habits</a></li>
        <li><a href="<c:url value="logCharts"/>">Charts</a></li>
      </ul>
    
    <h3>BMI Log: </h3>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Week Starts Date</th> 
					<th>Height</th> 
					<th>Weight</th> 
					<th>BMI Value</th> 
					<th>BMI Category  </th> 
					<th>Score <a target="_blank" href="<c:url value="/resources/info/bmi_category.jsp" />"><span class="glyphicon glyphicon-info-sign"></span></a></th>
				</tr>
			</thead>
			<tbody>
   			 <c:forEach items="${model.allBmiLog}" var="log">
  		  		<tr>
    				<td><c:out value="${log.startDate}" /></td> 
    				<td><c:out value="${log.height}" /></td> 
    				<td><c:out value="${log.weight}" /></td> 
  		  			<td class="bmiValue"><c:out value="${log.bmiValue}" /></td>
    				<td><c:out value="${log.category}" /></td>
    				<td><c:out value="${log.score}" /></td>
    	  		</tr> 
   			 </c:forEach>
   			 </tbody>
		</table>
	<br>


	<h3>Habits Log</h3>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Week Starts Date</th> 
					<th>Number of Days Eating Vegetables and Fruits</th> 
					<th>Number of Days Drinking</th> 
					<th>Number of Days Smoking</th> 
					<th>Score <a target="_blank" href="<c:url value="/resources/info/habits_score.jsp" />"><span class="glyphicon glyphicon-info-sign"></span></a></th>
				</tr>
   			</thead>
   			<tbody>
   			 	<c:forEach items="${model.allHabitsLog}" var="log">
  			  		<tr>
    					<td><c:out value="${log.startDate}" /></td> 
    					<td style="color:green"><c:out value="${log.eatingWellDays}" /></td> 
    					<td style="color:red"><c:out value="${log.drinkingDays}" /></td> 
    					<td style="color:red"><c:out value="${log.smokingDays}" /></td>
    					<td><c:out value="${log.score}" /></td>
   			 		</tr> 
   			 	</c:forEach>
   			 </tbody>
		</table>
		
		<br>
		
		<h3>Total: (${model.habitsCounter.numOfDays} days)</h3>
				<table class="table table-striped">
			<thead>
				<tr>
					<th></th> 
					<th>Eating Vegetables and Fruits</th> 
					<th>Drinking</th> 
					<th>Smoking</th> 
	
				</tr>
   			</thead>
   			<tbody>
  			  		<tr>
						<td>YES</td> 
						<td>${model.habitsCounter.sumEating}</td>
						<td>${model.habitsCounter.sumDrinking}</td>
						<td>${model.habitsCounter.sumSmoking}</td>
   			 		</tr> 
   			 		<tr>
						<td>NO</td> 
						<td>${model.habitsCounter.sumNotEating}</td>
						<td>${model.habitsCounter.sumNotDrinking}</td>
						<td>${model.habitsCounter.sumNotSmoking}</td>
   			 		</tr> 
   			 </tbody>
		</table>
<br>
    

  </div>
  <%@ include file="include/footer.jsp"%>


</body>
</html>