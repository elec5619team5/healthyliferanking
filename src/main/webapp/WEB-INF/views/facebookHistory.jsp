<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Facebook Component</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<%@ include file="include/stylesheet.jsp"%>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {

    	  var data = new google.visualization.DataTable();
    	  // Add columns
        data.addColumn('string', 'Date');
        data.addColumn('number', 'Score');
        
        
        <c:forEach items="${facebookDataList}" var="f">
        data.addRows([
                      ['${f.recordDate}', ${f.totalScore}],

                    ]);
        	
        </c:forEach>

        var options = {
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

        chart.draw(data, options);
      }
    </script>

</head>
<body>

<%@ include file="include/header.jsp"%>
	<div id="main" class="container">
	<h1>Facebook</h1>
	
	    <ul class="nav nav-tabs" role="tablist">
        <li><a href="<c:url value="/facebookProfile"/>">Profile</a></li>
        <li><a href="<c:url value="/facebookScore"/>">Score Details</a></li>
        <li class="active"><a href="<c:url value="/facebookHistory"/>">Score History</a></li>
      </ul>
      <h2>Facebook Scores History Line Chart</h2>
      <div id="chart_div"></div>
	</div>
    <%@ include file="include/footer.jsp"%>
  <%@ include file="include/script.jsp"%>	
</body>
</html>