<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
  uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Healthy Life Ranking</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="include/stylesheet.jsp"%>
</head>
<body>


  <%@ include file="include/header.jsp"%>
  <div id="main" class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h1>
            <span class="glyphicon glyphicon-user"></span> Dashboard
          </h1>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-beer fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge">
                      <span class="bmi score"><c:out value="${bmiScore}" /></span>
                      / 25
                    </div>
                    <div>BMI&Habbit</div>
                  </div>
                </div>
              </div>
              <a href="<c:url value="bhl"/>">
                <div class="panel-footer">
                  <span class="pull-left">View Details</span> <span
                    class="pull-right"><i
                    class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-dark">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-steam fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge">
                      <span class="steam score"><c:out value="${steamScore}" /></span>
                      / 25
                    </div>
                    <div>Steam</div>
                  </div>
                </div>
              </div>
              <a href="<c:url value="/updatesteam"/>">
                <div class="panel-footer">
                  <span class="pull-left">View Details</span> <span
                    class="pull-right"><i
                    class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-facebook-square fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge">
                      <span class="facebook score"><c:out value="${facebookScore}" /></span>
                      / 25
                    </div>
                    <div>Facebook</div>
                  </div>
                </div>
              </div>
              <a href="<c:url value="/facebook.htm"/>">
                <div class="panel-footer">
                  <span class="pull-left">View Details</span> <span
                    class="pull-right"><i
                    class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-bicycle fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge">
                      <span class="fitbit score"><c:out value="${fitbitScore}" /></span>
                      / 25
                    </div>
                    <div>Fitbit</div>
                  </div>
                </div>
              </div>
              <a href="<c:url value="/fitbitApiAuth"/>">
                <div class="panel-footer">
                  <span class="pull-left">View Details</span> <span
                    class="pull-right"><i
                    class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-9">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4><i class="fa fa-pie-chart"></i>&nbsp;Scores</h4>
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                <canvas id="chart"></canvas>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4><i class="fa fa-trophy"></i>&nbsp;Leaderboard</h4>
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">

                  <ol class="list-group">
                    <c:forEach items="${leaderboard}" var="leader">
                      <li class="list-group-item"><c:out value="${leader.second}"/><span class="pull-right"><c:out value="${leader.first}"/></span></li>
                    </c:forEach>
                  </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <%@ include file="include/footer.jsp"%>
  <%@ include file="include/script.jsp"%>
  <script src="<c:url value="/resources/js/chart.min.js" />"></script>
  <script type="text/javascript">
  $(function(){
    var bmi_score = parseInt($(".bmi.score").text());
    var steam_score = parseInt($(".steam.score").text());
    var facebook_score = parseInt($(".facebook.score").text());
    var fitbit_score = parseInt($(".fitbit.score").text());
    
    Chart.defaults.global.responsive = true;
    var ctx = $("#chart").get(0).getContext("2d");

      var data = [{
          value: steam_score,
          color: "#171a21",
          highlight: "#305d7d",
          label: "Steam"
        }, {
          value: facebook_score,
          color: "#428bca",
          highlight: "#3b5998",
          label: "Facebook"
        }, {
          value: fitbit_score,
          color: "#d9534f",
          highlight: "#FE814B",
          label: "Fitbit"
        }, {
          value: bmi_score,
          color: "#5cb85c",
          highlight: "#AEF370",
          label: "BMI&Habbit"
        }]
    var myDoughnutChart = new Chart(ctx).Doughnut(data);
  });
</script>
</body>
</html>