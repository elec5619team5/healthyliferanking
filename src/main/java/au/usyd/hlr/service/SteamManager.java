package au.usyd.hlr.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.hlr.domain.User;
import au.usyd.hlr.domain.steamModel;

@Service(value = "steamManager")
@Transactional
public class SteamManager implements Scorable{
	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<steamModel> getAllSteamLog(User user) {		
		return this.sessionFactory.getCurrentSession().createQuery(
						"FROM steamModel as asteam WHERE asteam.user.username =:username")
				.setString("username", user.getUsername()).list();
	}

	public void addSteam(steamModel asteam) {
		this.sessionFactory.getCurrentSession().save(asteam);
	}

	@Override
	public int getScore(User user) {
		// TODO Auto-generated method stub
		List<steamModel> steamInfo;
		steamInfo=getAllSteamLog(user);
		int totalScore=0;
		for(int i=0;i<steamInfo.size();i++){
			totalScore+=steamInfo.get(i).getScore();
		}
		if(totalScore==0){
			return 0;
		}
		return totalScore/steamInfo.size();
	}

}
