package au.usyd.hlr.service;

import au.usyd.hlr.domain.User;

public interface Scorable {
  public int getScore(User user);
}
