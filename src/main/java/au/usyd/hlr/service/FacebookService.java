package au.usyd.hlr.service;

import java.util.Date;
import java.util.List;

import au.usyd.hlr.domain.FacebookData;


public interface FacebookService {
	List<FacebookData> getFacebookDataList();
	void getUserProfile(String accessToken);
	void getNumberOfStatuses(String accessToken);
	void getNumberOfFriends(String accessToken);
	void getNumberOfPhotos(String accessToken);
	void saveFacebookData(Date currentDate);
	void updateFacebookData();
	FacebookData getLastestRecord();
	
}

