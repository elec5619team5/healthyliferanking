package au.usyd.hlr.service;

import java.util.List;

import au.usyd.hlr.domain.User;

public interface UserManager {
  public void addUser(User user);
  public void updateUser(User user);
  public void deleteUserByUsername(String username);
  public User getUserByUsername(String username);
  public User getCurrentUser();
  public List<User> getUsers();
}
