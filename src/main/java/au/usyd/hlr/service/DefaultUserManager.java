package au.usyd.hlr.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.hlr.dao.UserDao;
import au.usyd.hlr.domain.User;

@Service(value="DefaultUserManager")
@Transactional
public class DefaultUserManager implements UserManager {
  
  @Autowired
  private UserDao userDao;
  
  public UserDao getUserDao() {
    return userDao;
  }

  @Override
  public void addUser(User user) {
    userDao.addUser(user);
  }

  @Override
  public void updateUser(User user) {
    userDao.updateUser(user);

  }

  @Override
  public void deleteUserByUsername(String username) {
    userDao.deleteUserByUsername(username);
  }
  
  public User getUserByUsername(String username) {
    return (User) userDao.loadUserByUsername(username);
  }

  @Override
  public User getCurrentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User currentUser = (User) auth.getPrincipal();
    return currentUser;
  }

  @Override
  public List<User> getUsers() {
    return this.userDao.getUsers();
  }

}
