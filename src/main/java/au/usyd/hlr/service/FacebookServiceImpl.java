package au.usyd.hlr.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.hlr.dao.FacebookDao;
import au.usyd.hlr.domain.FacebookData;
import au.usyd.hlr.domain.User;

@Service(value = "FacebookServiceValue")
@Transactional
public class FacebookServiceImpl implements FacebookService, Scorable {


	private static final String UrlHost = "https://graph.facebook.com/";
	private String id;
	private String name;
	private String birthday;
	private String gender;
	private int statusNumber;
	private int friendNumber;
	private int photoNumber;
	
	@Autowired
	private FacebookDao facebookDao;


	public void setFacebookDao(FacebookDao facebookDao) {
		this.facebookDao = facebookDao;
	}

	public void getUserProfile(String accessToken) {
		FacebookTemplate facebook = new FacebookTemplate(accessToken);

		FacebookProfile userProfile = facebook.userOperations()
				.getUserProfile();
		id = userProfile.getId();
		name = userProfile.getName();
		birthday = userProfile.getBirthday();
		gender = userProfile.getGender();

	}

	public void getNumberOfStatuses(String accessToken) {
		if (accessToken != null && this.id != null) {
			String url = UrlHost + this.id
					+ "?fields=statuses%7Bid%7D&access_token=" + accessToken;

			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Create a method instance.
				HttpGet httpget = new HttpGet(url);
				HttpResponse httpresponse = httpclient.execute(httpget);
				HttpEntity entity = httpresponse.getEntity();

				String responseStr = null;
				if (entity != null) {
					responseStr = EntityUtils.toString(entity);
					try {

						JSONObject o = new JSONObject(responseStr);

					
						JSONObject statusObject = o.getJSONObject("statuses");
						JSONArray dataObject = statusObject
								.getJSONArray("data");
						statusNumber = dataObject.length();
					} catch (Exception e) {
						// TODO Auto-generated catch block
					}

				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void getNumberOfFriends(String accessToken) {
		if (accessToken != null && this.id != null) {
			String url = UrlHost + this.id + "?fields=friends&access_token="
					+ accessToken;

			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Create a method instance.
				HttpGet httpget = new HttpGet(url);
				HttpResponse httpresponse = httpclient.execute(httpget);
				HttpEntity entity = httpresponse.getEntity();

				String responseStr = null;
				if (entity != null) {
					responseStr = EntityUtils.toString(entity);
					try {

						JSONObject o = new JSONObject(responseStr);
						
						JSONObject friendsObj = o.getJSONObject("friends")
								.getJSONObject("summary");
						
						friendNumber = friendsObj.getInt("total_count");
					} catch (Exception e) {
						// TODO Auto-generated catch block

					}

				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void getNumberOfPhotos(String accessToken) {
		if (accessToken != null && this.id != null) {
			String url = UrlHost + this.id + "?fields=photos&access_token="
					+ accessToken;

			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Create a method instance.
				HttpGet httpget = new HttpGet(url);
				HttpResponse httpresponse = httpclient.execute(httpget);
				HttpEntity entity = httpresponse.getEntity();

				String responseStr = null;
				if (entity != null) {
					responseStr = EntityUtils.toString(entity);
					try {

						JSONObject o = new JSONObject(responseStr);
						JSONObject photoObj = o.getJSONObject("photos");
						photoNumber = photoObj.getJSONArray("data").length();
					} catch (Exception e) {
						// TODO Auto-generated catch block

					}

				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public List<FacebookData> getFacebookDataList()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    User currentUser = (User) auth.getPrincipal();
	    
	    return facebookDao.getFacebookDataByUser(currentUser);
		
	}

	@Override
	public FacebookData getLastestRecord() {
		// TODO Auto-generated method stub
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    User currentUser = (User) auth.getPrincipal();
	    
	    return facebookDao.getLastestDataByUser(currentUser);
		
	}
	
	public FacebookData createFacebookData(Date currentDate){
		
		int newFriendNumber = 0;
		int newStatusNumber = 0;
		int newPhotoNumber = 0;
		
		FacebookData newFacebookData = new FacebookData();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    User currentUser = (User) auth.getPrincipal();
	    newFacebookData.setUser(currentUser);
		newFacebookData.setName(name);
		newFacebookData.setFacebookId(id);
		newFacebookData.setBirthday(birthday);
		newFacebookData.setGender(gender);
		newFacebookData.setFriendNumber(friendNumber);
		newFacebookData.setStatusNumber(statusNumber);
		newFacebookData.setPhotoNumber(photoNumber);
		newFacebookData.setRecordDate(currentDate);
		if(facebookDao.getFacebookDataByUser(currentUser).size()>0)
		{
			newFriendNumber = friendNumber - facebookDao.getLastestDataByUser(currentUser).getFriendNumber();
			newStatusNumber = statusNumber - facebookDao.getLastestDataByUser(currentUser).getStatusNumber();
			newPhotoNumber = photoNumber - facebookDao.getLastestDataByUser(currentUser).getPhotoNumber();
			
			newFacebookData.setNewFriendNumber(newFriendNumber);
			newFacebookData.setNewStatusNumber(newStatusNumber);
			newFacebookData.setNewPhotoNumber(newPhotoNumber);
			double totalScore = newFacebookData.getFriendScore()
					+newFacebookData.getStatusScore()+newFacebookData.getPhotoScore();
			newFacebookData.setTotalScore(totalScore);
		}
		else
		{
			newFacebookData.setNewFriendNumber(friendNumber);
			newFacebookData.setNewStatusNumber(statusNumber);
			newFacebookData.setNewPhotoNumber(photoNumber);
			double totalScore = newFacebookData.getFriendScore()
					+newFacebookData.getStatusScore()+newFacebookData.getPhotoScore();
			newFacebookData.setTotalScore(totalScore);
		}
		return newFacebookData;
	
		
	}
	
	@Override
	public void saveFacebookData(Date currentDate){
		facebookDao.addFacebookData(this.createFacebookData(currentDate));
		
	}

	@Override
	public void updateFacebookData() {
		// TODO Auto-generated method stub
		FacebookData updateFacebookData = this.getLastestRecord();
		int newFriendNumber = friendNumber - updateFacebookData.getFriendNumber() + updateFacebookData.getNewFriendNumber();
		int newStatusNumber = statusNumber - updateFacebookData.getStatusNumber() + updateFacebookData.getNewStatusNumber();
		int newPhotoNumber = photoNumber - updateFacebookData.getPhotoNumber()+ updateFacebookData.getNewPhotoNumber();
		
		updateFacebookData.setNewFriendNumber(newFriendNumber);
		updateFacebookData.setNewStatusNumber(newStatusNumber);
		updateFacebookData.setNewPhotoNumber(newPhotoNumber);
		updateFacebookData.setFriendNumber(friendNumber);
		updateFacebookData.setStatusNumber(statusNumber);
		updateFacebookData.setPhotoNumber(photoNumber);
		double totalScore = updateFacebookData.getFriendScore()
				+updateFacebookData.getStatusScore()+updateFacebookData.getPhotoScore();
		updateFacebookData.setTotalScore(totalScore);
		facebookDao.updateFacebookData(updateFacebookData);	
	}

	@Override
	public int getScore(User user) {
		// TODO Auto-generated method stub
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    User currentUser = (User) auth.getPrincipal();
	    List<FacebookData> facebookData = facebookDao.getFacebookDataByUser(currentUser);
	    if(facebookData.size()==0)
	    {
		return 0;
	    }
	    else
	    	return (int)facebookDao.getLastestDataByUser(currentUser).getTotalScore();
	}
	

	
	



}
