package au.usyd.hlr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.hlr.dao.FitbitDao;
import au.usyd.hlr.domain.Activities;
import au.usyd.hlr.domain.Bloodpressure;
import au.usyd.hlr.domain.Glucose;
import au.usyd.hlr.domain.HeartRate;
import au.usyd.hlr.domain.Sleep;
import au.usyd.hlr.domain.User;

@Service(value = "DefaultFitbitManger")
public class FitbitMangerImpl implements FitbitManager, Scorable {

	@Autowired
	private FitbitDao fitbitdao;

	public FitbitDao getFitbitdao() {
		return fitbitdao;
	}

	public void setFitbitdao(FitbitDao fitbitdao) {
		this.fitbitdao = fitbitdao;
	}

	@Override
	public int addFitbitData(Activities activities,
			Bloodpressure bloodpressure, Glucose glucose, HeartRate heartrate,
			Sleep sleep) {
		// TODO Auto-generated method stub
		double mark = 0, totalmark = 0;
		if (1500 <= activities.getCalories()
				&& activities.getCalories() <= 2500) {
			mark = 5;
		} else if (1000 <= activities.getCalories()
				&& activities.getCalories() <= 1500
				|| 2500 <= activities.getCalories()
				&& activities.getCalories() <= 3000) {
			mark = 3;
		} else if (800 <= activities.getCalories()) {
			mark = 1;
		} else if (3000 <= activities.getCalories()
				&& activities.getCalories() <= 4000) {
			mark = 2;
		}
		totalmark = totalmark + mark;
		activities.setMark(mark);

		mark = 0;
		if (115 <= bloodpressure.getAvebloodpressure()
				&& bloodpressure.getAvebloodpressure() <= 125) {
			mark = 5;
		} else if (110 <= bloodpressure.getAvebloodpressure()
				&& bloodpressure.getAvebloodpressure() <= 115) { 
			mark = 4;
		} else if (125 <= bloodpressure.getAvebloodpressure()
				&& bloodpressure.getAvebloodpressure() <= 130) {
			mark = 2;
		} else if (130 <= bloodpressure.getAvebloodpressure()
				|| bloodpressure.getAvebloodpressure() <= 110) {
			mark = 1;
		}
		bloodpressure.setMark(mark);
		totalmark = totalmark + mark;
		mark = 0;
		if (4 == (int)glucose.getAveglucose()) {
			mark = 5;
		} else if (5 == (int)glucose.getAveglucose()) {
			mark = 4;
		} else if (3 == (int)glucose.getAveglucose()) {
			mark = 3;
		} else if (6 == (int)glucose.getAveglucose()) {
			mark = 2;
		} else {
			mark = 1;
		}
		glucose.setMark(mark);
		totalmark = totalmark + mark;
		mark = 0;
		if (65 <= heartrate.getNormalheartrate()
				&& heartrate.getNormalheartrate() <= 75) {
			mark = 5;
		} else if (60 <= heartrate.getNormalheartrate()
				&& heartrate.getNormalheartrate() <= 65) {
			mark = 4;
		} else if (75 <= heartrate.getNormalheartrate()
				&& heartrate.getNormalheartrate() <= 80) {
			mark = 3;
		} else if (80 <= heartrate.getNormalheartrate()
				&& heartrate.getNormalheartrate() <= 60) {
			mark = 1;
		}
		heartrate.setMark(mark);
		totalmark = totalmark + mark;
		mark = 5;
		if ((int) (sleep.getTimeinbed() / 60) >= 9
				&& (int) (sleep.getTimeinbed() / 60) < 11) {
			mark = mark - 3;
		} else if ((int) (sleep.getTimeinbed() / 60) >= 11) {
			mark = 0;
		} else if (4 < (int) (sleep.getTimeinbed() / 60)
				&& (int) (sleep.getTimeinbed() / 60) <= 6) {
			mark = 2;
		} else if ((int) (sleep.getTimeinbed() / 60) <= 4) {
			mark = 0;
		}
		sleep.setMark(mark);
		totalmark = totalmark + mark;
		this.fitbitdao.addFitbitData(activities, bloodpressure, glucose,
				heartrate, sleep);
		return (int)totalmark;
	}

	@Override
	public Activities getActivitiesByUser(User user) {
		// TODO Auto-generated method stub
		return fitbitdao.getActivitiesByUser(user);
	}

	@Override
	public Bloodpressure getBloodpressureByUser(User user) {
		// TODO Auto-generated method stub
		return fitbitdao.getBloodpressureByUser(user);
	}

	@Override
	public Glucose getGlucoseByUser(User user) {
		// TODO Auto-generated method stub
		return fitbitdao.getGlucoseByUser(user);
	}

	@Override
	public HeartRate getHeartRateByUser(User user) {
		// TODO Auto-generated method stub
		return fitbitdao.getHeartRateByUser(user);
	}

	@Override
	public Sleep getSleepByUser(User user) {
		return fitbitdao.getSleepByUser(user);
	}

	@Override
	public int getScore(User user) {
		// TODO Auto-generated method stub
		int totalscore = 0;

		Sleep sleepbuffer = fitbitdao.getSleepByUser(user);
		HeartRate heartratebuffer = fitbitdao.getHeartRateByUser(user);
		Glucose glucosebuffer = fitbitdao.getGlucoseByUser(user);
		Bloodpressure bloodpressurebuffer = fitbitdao
				.getBloodpressureByUser(user);
		Activities activitiesbuffer = fitbitdao.getActivitiesByUser(user);
		if (sleepbuffer != null && heartratebuffer != null
				&& glucosebuffer != null && bloodpressurebuffer != null
				&& activitiesbuffer != null) {

			totalscore = (int) sleepbuffer.getMark()
					+ (int) heartratebuffer.getMark()
					+ (int) glucosebuffer.getMark()
					+ (int) bloodpressurebuffer.getMark()
					+ (int) activitiesbuffer.getMark();
		}
		return totalscore;
	}

}
