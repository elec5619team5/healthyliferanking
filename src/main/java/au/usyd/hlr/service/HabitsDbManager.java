package au.usyd.hlr.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.hlr.domain.BMILog;
import au.usyd.hlr.domain.HabitsLog;
import au.usyd.hlr.domain.User;

@Service(value="HabitsLogManager")
@Transactional
public class HabitsDbManager implements Scorable{
	
private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<HabitsLog> getAllHabitsLog(User user) {
		//return this.sessionFactory.getCurrentSession().createQuery("FROM HabitsLog").list();
		return this.sessionFactory.getCurrentSession().createQuery("FROM HabitsLog as log WHERE log.user.username =:username")
				.setString("username", user.getUsername()).list();
	}
	
	public void addHabitsLog(HabitsLog log) {
		this.sessionFactory.getCurrentSession().save(log);
	}

	@Override
	public int getScore(User user) {
		int total=0;
		List<HabitsLog> habitsLogList = this.sessionFactory.getCurrentSession().createQuery("FROM HabitsLog as log WHERE log.user.username =:username").setString("username", user.getUsername()).list();
		HabitsLog lastHabitsLog = new HabitsLog();
		if(habitsLogList.size()>0){
			lastHabitsLog = habitsLogList.get(habitsLogList.size()-1);
			total += lastHabitsLog.getScore();
		}
		
		return total;
	}

}
