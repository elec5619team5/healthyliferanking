package au.usyd.hlr.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.hlr.domain.BMILog;
import au.usyd.hlr.domain.User;

@Service(value="BmiLogManager")
@Transactional
public class BmiDbManager implements Scorable{
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<BMILog> getAllBmiLog(User user) {
		
		return this.sessionFactory.getCurrentSession().createQuery("FROM BMILog as log WHERE log.user.username =:username").setString("username", user.getUsername()).list();
	}
	
	public void addBmiLog(BMILog bmiLog) {
		this.sessionFactory.getCurrentSession().save(bmiLog);
	}
	
	public BMILog getLatestLog(User user){
		
		List<BMILog> BmiLogList = this.sessionFactory.getCurrentSession().createQuery("FROM BMILog as log WHERE log.user.username =:username").setString("username", user.getUsername()).list();
		BMILog lastBmiLog = new BMILog();
		if(BmiLogList.size()>0){
			lastBmiLog = BmiLogList.get(BmiLogList.size()-1);
		}
		return lastBmiLog;
	}

	@Override
	public int getScore(User user) {
		
		int total=0;
		List<BMILog> BmiLogList = this.sessionFactory.getCurrentSession().createQuery("FROM BMILog as log WHERE log.user.username =:username").setString("username", user.getUsername()).list();
		BMILog lastBmiLog = new BMILog();
		if(BmiLogList.size()>0){
			lastBmiLog = BmiLogList.get(BmiLogList.size()-1);
			total += lastBmiLog.getScore();
		}
		
		return total;
	}

}
