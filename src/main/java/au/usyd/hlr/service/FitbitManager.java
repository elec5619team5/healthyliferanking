package au.usyd.hlr.service;

import au.usyd.hlr.domain.Activities;
import au.usyd.hlr.domain.Bloodpressure;
import au.usyd.hlr.domain.Glucose;
import au.usyd.hlr.domain.HeartRate;
import au.usyd.hlr.domain.Sleep;
import au.usyd.hlr.domain.User;

public interface FitbitManager {
	public int addFitbitData(Activities activities, Bloodpressure bloodpressure, Glucose glucose, HeartRate heartrate, Sleep sleep);
	
	public Activities getActivitiesByUser(User user);
	public Bloodpressure getBloodpressureByUser(User user);
	public Glucose getGlucoseByUser(User user);
	public HeartRate getHeartRateByUser(User user);
	public Sleep getSleepByUser(User user);
	public int getScore(User user);
}
