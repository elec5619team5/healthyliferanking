package au.usyd.hlr.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.hlr.domain.Activities;
import au.usyd.hlr.domain.Bloodpressure;
import au.usyd.hlr.domain.Glucose;
import au.usyd.hlr.domain.HeartRate;
import au.usyd.hlr.domain.Sleep;
import au.usyd.hlr.domain.User;

@Repository(value = "fitbitDao")
@Transactional
public class FitbitDaoImpl implements FitbitDao {

	SessionFactory sessionFactory;

	@Override
	public void addFitbitData(Activities activities,
			Bloodpressure bloodpressure, Glucose glucose, HeartRate heartrate,
			Sleep sleep) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(activities);
		this.sessionFactory.getCurrentSession().save(bloodpressure);
		this.sessionFactory.getCurrentSession().save(glucose);
		this.sessionFactory.getCurrentSession().save(heartrate);
		this.sessionFactory.getCurrentSession().save(sleep);
	}

	@Autowired
	public void setSessionFactoty(SessionFactory sessionFactoty) {
		this.sessionFactory = sessionFactoty;
	}

	@Override
	public Activities getActivitiesByUser(User user) {
		// TODO Auto-generated method stub
		if (user != null) {
			String username = user.getUsername();

			List<Activities> activitiesList = this.sessionFactory
					.getCurrentSession()
					.createQuery(
							"FROM Activities as f where f.user.username = :username")
					.setString("username", username).list();

			if (activitiesList != null && activitiesList.size() >= 1) {
				Date newDate = activitiesList.get(0).getDate();
				int newestRecord = 0;

				for (int i = 1; i < activitiesList.size(); i++) {
					if (activitiesList.get(i).getDate().after(newDate)) {
						newDate = activitiesList.get(i).getDate();
						newestRecord = i;
					}
				}
				System.out.println(activitiesList.size());

				return activitiesList.get(newestRecord);
			}
		}
		return null;
	}

	@Override
	public Bloodpressure getBloodpressureByUser(User user) {
		// TODO Auto-generated method stub
		if (user != null) {
			String username = user.getUsername();
			List<Bloodpressure> bloodpressureList = this.sessionFactory
					.getCurrentSession()
					.createQuery(
							"FROM Bloodpressure as f where f.user.username = :username")
					.setString("username", username).list();

			if (bloodpressureList != null && bloodpressureList.size() >= 1) {
				Date newDate = bloodpressureList.get(0).getDate();
				int newestRecord = 0;

				for (int i = 1; i < bloodpressureList.size(); i++) {
					if (bloodpressureList.get(i).getDate().after(newDate)) {
						newDate = bloodpressureList.get(i).getDate();
						newestRecord = i;
					}
				}
				System.out.println(bloodpressureList.size());

				return bloodpressureList.get(newestRecord);
			}
		}
		return null;
	}

	@Override
	public Glucose getGlucoseByUser(User user) {
		// TODO Auto-generated method stub
		if (user != null) {
			String username = user.getUsername();
			List<Glucose> glucoseList = this.sessionFactory
					.getCurrentSession()
					.createQuery(
							"FROM Glucose as f where f.user.username = :username")
					.setString("username", username).list();
			if (glucoseList != null && glucoseList.size() >= 1) {
				Date newDate = glucoseList.get(0).getDate();
				int newestRecord = 0;

				for (int i = 1; i < glucoseList.size(); i++) {
					if (glucoseList.get(i).getDate().after(newDate)) {
						newDate = glucoseList.get(i).getDate();
						newestRecord = i;
					}
				}
				System.out.println(glucoseList.size());

				return glucoseList.get(newestRecord);
			}
		}
		return null;
	}

	@Override
	public HeartRate getHeartRateByUser(User user) {
		// TODO Auto-generated method stub
		if (user != null) {
			String username = user.getUsername();
			List<HeartRate> heartrateList = this.sessionFactory
					.getCurrentSession()
					.createQuery(
							"FROM HeartRate as f where f.user.username = :username")
					.setString("username", username).list();
			if (heartrateList != null && heartrateList.size() >= 1) {
				Date newDate = heartrateList.get(0).getDate();
				int newestRecord = 0;

				for (int i = 1; i < heartrateList.size(); i++) {
					if (heartrateList.get(i).getDate().after(newDate)) {
						newDate = heartrateList.get(i).getDate();
						newestRecord = i;
					}
				}
				System.out.println(heartrateList.size());

				return heartrateList.get(newestRecord);
			}
		}
		return null;
	}

	@Override
	public Sleep getSleepByUser(User user) {
		// TODO Auto-generated method stub
		if (user != null) {
			String username = user.getUsername();
			List<Sleep> sleepList = this.sessionFactory
					.getCurrentSession()
					.createQuery(
							"FROM Sleep as f where f.user.username = :username")
					.setString("username", username).list();

			if (sleepList != null && sleepList.size() >= 1) {

				Date newDate = sleepList.get(0).getDate();
				int newestRecord = 0;

				for (int i = 1; i < sleepList.size(); i++) {
					System.out.println(sleepList.size()
							+ sleepList.get(i).getMark());
					if (sleepList.get(i).getDate().after(newDate)) {
						newDate = sleepList.get(i).getDate();
						newestRecord = i;
					}
				}
				System.out.println(sleepList.size());

				return sleepList.get(newestRecord);
			}
		}
		return null;
	}
}
