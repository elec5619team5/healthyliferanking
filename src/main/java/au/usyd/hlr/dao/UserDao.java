package au.usyd.hlr.dao;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

import au.usyd.hlr.domain.User;

public interface UserDao {
  public void addUser(User user);
  public void updateUser(User user);
  public void deleteUserByUsername(String username);
  public UserDetails loadUserByUsername(String username);
  public List<User> getUsers();
  
}
