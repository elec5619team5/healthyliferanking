package au.usyd.hlr.dao;

import java.util.List;

import au.usyd.hlr.domain.FacebookData;
import au.usyd.hlr.domain.User;

public interface FacebookDao {
	
	public void addFacebookData(FacebookData facebookData);
	public void updateFacebookData(FacebookData facebookData);
	
	public List<FacebookData> getFacebookDataByUser(User user);
	public FacebookData getLastestDataByUser(User user);

}
