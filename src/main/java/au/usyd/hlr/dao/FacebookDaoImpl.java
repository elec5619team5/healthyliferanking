package au.usyd.hlr.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.hlr.dao.FacebookDao;
import au.usyd.hlr.domain.FacebookData;
import au.usyd.hlr.domain.User;

@Repository(value="facebookDao")
@Transactional
public class FacebookDaoImpl implements FacebookDao{
	
	SessionFactory sessionFactoty;

	@Override
	public void addFacebookData(FacebookData facebookData) {
		// TODO Auto-generated method stub
		this.sessionFactoty.getCurrentSession().save(facebookData);	
	}
	


	@Override
	public void updateFacebookData(FacebookData facebookData) {
		// TODO Auto-generated method stub
		
		this.sessionFactoty.getCurrentSession().merge(facebookData);
		
	}
	

	@Autowired
	public void setSessionFactoty(SessionFactory sessionFactoty) {
		this.sessionFactoty = sessionFactoty;
	}


	@Override
	public List<FacebookData> getFacebookDataByUser(User user) {
		// TODO Auto-generated method stub
		String username = user.getUsername();
		List<FacebookData> facebookDataList = this.sessionFactoty.getCurrentSession().createQuery("FROM FacebookData as f where f.user.username = :username")
            .setString("username", username).list();
		
		FacebookData preData;
		for(int i =0; i<facebookDataList.size();i++)
		{
			for(int j=0; j<facebookDataList.size()-1; j++)
				if(facebookDataList.get(j).getRecordDate().after(facebookDataList.get(j+1).getRecordDate()))
				{
					preData = facebookDataList.get(j);
					facebookDataList.set(j, facebookDataList.get(j+1));
					facebookDataList.set(j+1, preData);
				}
			preData =null;
		}
		return facebookDataList;
		
	}


	@Override
	public FacebookData getLastestDataByUser(User user) {
		// TODO Auto-generated method stub
		String username = user.getUsername();
		List<FacebookData> facebookDataList = this.sessionFactoty.getCurrentSession().createQuery("FROM FacebookData as f where f.user.username = :username")
				.setString("username", username).list();
		
		Date lastestFacebookDataDate = facebookDataList.get(0).getRecordDate();
		int lastestFacebookData = 0;
		
		for(int i=1; i< facebookDataList.size();i++)
		{
			if(facebookDataList.get(i).getRecordDate().after(lastestFacebookDataDate))
			{
				lastestFacebookDataDate = facebookDataList.get(i).getRecordDate();
				lastestFacebookData = i;
			}
		}
		
		return facebookDataList.get(lastestFacebookData);
		
	}

	
	
	
	

}
