package au.usyd.hlr.domain;

import java.util.List;

public class HabitsCounter {

	private int numOfDays = 0;
	private int sumEating = 0;
	private int sumDrinking = 0;
	private int sumSmoking = 0;
	private int sumNotEating = 0;
	private int sumNotDrinking = 0;
	private int sumNotSmoking = 0;
	List<HabitsLog> allHabitsLog;
	
	public HabitsCounter(List<HabitsLog> allHabitsLog) {
		super();
		this.allHabitsLog = allHabitsLog;
	}
	
	public void countTotal(){
		this.numOfDays = this.allHabitsLog.size()*7;
		
		 for(int i=0;i<this.allHabitsLog.size();i++){
			 this.sumEating += allHabitsLog.get(i).getEatingWellDays();
		 }
		 
		 for(int i=0;i<this.allHabitsLog.size();i++){
			 this.sumDrinking += allHabitsLog.get(i).getDrinkingDays();
		 }
		 
		 for(int i=0;i<this.allHabitsLog.size();i++){
			 sumSmoking += allHabitsLog.get(i).getSmokingDays();
		 }
		 
		 this.sumNotEating = numOfDays - sumEating;
		 this.sumNotDrinking = numOfDays - sumDrinking;
		 this.sumNotSmoking = numOfDays - sumSmoking;
	}

	public int getNumOfDays() {
		return numOfDays;
	}

	public void setNumOfDays(int numOfDays) {
		this.numOfDays = numOfDays;
	}

	public int getSumEating() {
		return sumEating;
	}

	public void setSumEating(int sumEating) {
		this.sumEating = sumEating;
	}

	public int getSumDrinking() {
		return sumDrinking;
	}

	public void setSumDrinking(int sumDrinking) {
		this.sumDrinking = sumDrinking;
	}

	public int getSumNotEating() {
		return sumNotEating;
	}

	public void setSumNotEating(int sumNotEating) {
		this.sumNotEating = sumNotEating;
	}

	public int getSumNotDrinking() {
		return sumNotDrinking;
	}

	public void setSumNotDrinking(int sumNotDrinking) {
		this.sumNotDrinking = sumNotDrinking;
	}

	public int getSumNotSmoking() {
		return sumNotSmoking;
	}

	public void setSumNotSmoking(int sumNotSmoking) {
		this.sumNotSmoking = sumNotSmoking;
	}

	public List<HabitsLog> getAllHabitsLog() {
		return allHabitsLog;
	}

	public void setAllHabitsLog(List<HabitsLog> allHabitsLog) {
		this.allHabitsLog = allHabitsLog;
	}

	public int getSumSmoking() {
		return sumSmoking;
	}

	public void setSumSmoking(int sumSmoking) {
		this.sumSmoking = sumSmoking;
	}
	
	
	
	
}
