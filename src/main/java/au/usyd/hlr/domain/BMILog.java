package au.usyd.hlr.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BMILog")
public class BMILog implements Serializable {

	@Id
	@GeneratedValue
	private int logID;

	@Column(name = "StartDate")
	private String startDate;

	@Column(name = "Height")
	private double height=0;

	@Column(name = "Weight")
	private int weight=0;

	@Column(name = "BmiValue")
	private double bmiValue;

	@Column(name = "Category")
	private String category;
	
	@Column(name = "Score")
	private int score;

	@ManyToOne
	@JoinColumn(name = "Username")
	private User user;

	public BMILog() {
		super();
	}

	public double calculateBmi() {
		double result = 0.0;
		result = this.weight / (height * height);
		return result;
	}

	public String determineBmiCategory() {
		String result = "unknown";
		if (this.bmiValue < 16)
			result = "Severe thinness";
		else if (this.bmiValue >= 16 && this.bmiValue < 17)
			result = "Moderate thinness";
		else if (this.bmiValue >= 17 && this.bmiValue < 18.5)
			result = "Mild thinness";
		else if (this.bmiValue >= 18.5 && this.bmiValue < 25)
			result = "Normal";
		else if (this.bmiValue >= 25 && this.bmiValue < 30)
			result = "Pre-obese";
		else if (this.bmiValue >= 30 && this.bmiValue < 35)
			result = "Obese class I";
		else if (this.bmiValue >= 35 && this.bmiValue <= 40)
			result = "Obese class II";
		else if (this.bmiValue > 40)
			result = "Obese class III";
		return result;
	}
	
	public void updateScore(){
		int newScore=0;
		
		if (this.bmiValue < 16)
			newScore = 0;
		else if (this.bmiValue >= 16 && this.bmiValue < 17)
			newScore = 3;
		else if (this.bmiValue >= 17 && this.bmiValue < 18.5)
			newScore = 7;
		else if (this.bmiValue >= 18.5 && this.bmiValue < 25)
			newScore = 10;
		else if (this.bmiValue >= 25 && this.bmiValue < 30)
			newScore = 7;
		else if (this.bmiValue >= 30 && this.bmiValue < 35)
			newScore = 3;
		else if (this.bmiValue >= 35 && this.bmiValue <= 40)
			newScore = 0;
		else if (this.bmiValue > 40)
			newScore = -1;
		
		this.setScore(newScore);
	}

	public int getLogID() {
		return logID;
	}

	public void setLogID(int logID) {
		this.logID = logID;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public double getBmiValue() {
		return bmiValue;
	}

	public void setBmiValue(double bmiValue) {
		this.bmiValue = bmiValue;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	

}
