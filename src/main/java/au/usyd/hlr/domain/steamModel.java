package au.usyd.hlr.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "SteamInfo")
public class steamModel {
	
	@Id
	@GeneratedValue
	private int logID;

	@Column(name = "CustomerName")
	private String customerName;

	@Column(name = "PlayingTime")
	private String playingTime;

	@Column(name = "RecordDate")
	private Date recordDate;

	@Column(name = "Score")
	private int score;

	@Column(name = "Suggestion")
	private String suggestion;
	
	@Column(name = "TotalScore")
	private int totalscore;
	
	public int getTotalscore() {
		return totalscore;
	}


	public void setTotalscore(int totalscore) {
		this.totalscore = totalscore;
	}


	public String getSuggestion() {
		return suggestion;
	}


	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}

	@ManyToOne
	@JoinColumn(name = "Username")
	private User user;
	
	public steamModel() {
		super();
	}

	
	public int getLogID() {
		return logID;
	}

	public void setLogID(int logID) {
		this.logID = logID;
	}
	

	public String getPlayingTime() {
		return playingTime;
	}

	public void setPlayingTime(String playingTime) {
		this.playingTime = playingTime;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public void setScore(int Score){
		this.score=Score;
	}
	
	public int getScore(){
		return score;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public void setCustomerName(String CustomerName){
		this.customerName=CustomerName;
	}
	
	public String getCustomerName(){
		return customerName;
	}
	
	
	
}
