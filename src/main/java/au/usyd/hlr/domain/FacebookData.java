package au.usyd.hlr.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Facebook")
public class FacebookData {
	
	@Id
	@GeneratedValue
	@Column(name="dataId")
	private Long dataId;

	@ManyToOne
	@JoinColumn(name="Userid")
	private User user;
	
	@Column(name="FacebookId")
	private String facebookId;
	
	
	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	@Column(name="Name")
	private String name;
	
	@Column(name="Birthday")
	private String birthday;
	
	@Column(name="Gender")
	private String gender;
	
	@Column(name="NewFriendNumber")
	private int newFriendNumber;
	
	@Column(name="NewStatusNumber")
	private int newStatusNumber;
	
	@Column(name="NewPhotoNumber")
	private int newPhotoNumber;
	
	public int getNewPhotoNumber() {
		return newPhotoNumber;
	}

	public void setNewPhotoNumber(int newPhotoNumber) {
		this.newPhotoNumber = newPhotoNumber;
	}

	@Column(name="FriendScore")
	private double friendScore;
	
	@Column(name="StatusScore")
	private double statusScore;
	
	@Column(name="PhotoScore")
	private double photoScore;
	
	@Column(name="TotalScore")
	private double totalScore;
	
	public int getNewFriendNumber() {
		return newFriendNumber;
	}

	public void setNewFriendNumber(int newFriendNumber) {
		this.newFriendNumber = newFriendNumber;
	}

	public int getNewStatusNumber() {
		return newStatusNumber;
	}

	public void setNewStatusNumber(int newStatusNumber) {
		this.newStatusNumber = newStatusNumber;
	}

	public double getFriendScore() {
		if(this.newFriendNumber<=5 && this.newFriendNumber >= 0)
		{
			this.friendScore = this.newFriendNumber * 1;
		}
		else
		{
			this.friendScore = 5;
		}
		return friendScore;
	}

	public void setFriendScore(double friendScore) {
		this.friendScore = friendScore;
	}

	public double getStatusScore() {
		
		if(0<=this.newStatusNumber && this.newStatusNumber<=20)
		{
			this.statusScore = this.newStatusNumber * 0.5;
		}
		else if(this.newStatusNumber >20 && this.newStatusNumber <=40)
		{
			 this.statusScore = 10 - (this.newStatusNumber-20) * 0.5;
		}
		else
		{
			this.statusScore = 0;
		}
		return statusScore;
	}

	public void setStatusScore(double statusScore) {
		this.statusScore = statusScore;
	}

	public double getPhotoScore() {
		
		if(this.newPhotoNumber >=0 && this.newPhotoNumber <=20)
		{
			this.photoScore = this.newPhotoNumber * 0.5;
		}
		else
		{
			this.photoScore = 10;
		}
		
		return photoScore;
	}

	public void setPhotoScore(double photoScore) {
		this.photoScore = photoScore;
	}

	public double getTotalScore() {
//		this.totalScore = this.friendScore + this.statusScore + this.photoScore;
		return totalScore;
	}

	public void setTotalScore(double totalScore) {
		this.totalScore = totalScore;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name="RecordDate")
	private Date recordDate;
	
	@Column(name="FriendNumber")
	private int friendNumber;
	
	@Column(name="PhotoNumber")
	private int photoNumber;
	
	@Column(name="StatusNumber")
	private int statusNumber;

	public Long getDataId() {
		return dataId;
	}

	public void setDataId(Long dataId) {
		this.dataId = dataId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public int getFriendNumber() {
		return friendNumber;
	}

	public void setFriendNumber(int friendNumber) {
		this.friendNumber = friendNumber;
	}

	public int getPhotoNumber() {
		return photoNumber;
	}

	public void setPhotoNumber(int photoNumber) {
		this.photoNumber = photoNumber;
	}

	public int getStatusNumber() {
		return statusNumber;
	}

	public void setStatusNumber(int statusNumber) {
		this.statusNumber = statusNumber;
	}
	
}
