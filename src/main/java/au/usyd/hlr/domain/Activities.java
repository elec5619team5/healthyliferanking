package au.usyd.hlr.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Activities")
public class Activities implements Serializable {

	@Id
	@GeneratedValue
	private int logID;
	
	@Column(name = "steps")
	private int steps;
	
	@Column(name = "distance")
	private double distance;
	
	@Column(name = "calories")
	private double calories;
	
	@Column(name = "floors")
	private int floors;
	
	@Column(name = "mark")
	private double mark;
	
	@Column(name = "date")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="Userid")
	private User user;
	
	public Activities() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Activities(int logID, int steps, double distance, double calories,
			int floors, double mark, Date date, User user) {
		super();
		this.logID = logID;
		this.steps = steps;
		this.distance = distance;
		this.calories = calories;
		this.floors = floors;
		this.mark = mark;
		this.date = date;
		this.user = user;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getLogID() {
		return logID;
	}
	public void setLogID(int logID) {
		this.logID = logID;
	}
	public int getSteps() {
		return steps;
	}
	public void setSteps(int steps) {
		this.steps = steps;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public double getCalories() {
		return calories;
	}
	public void setCalories(double calories) {
		this.calories = calories;
	}
	public int getFloors() {
		return floors;
	}
	public void setFloors(int floors) {
		this.floors = floors;
	}
	public double getMark() {
		return mark;
	}
	public void setMark(double mark) {
		this.mark = mark;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Activities [logID=" + logID + ", steps=" + steps
				+ ", distance=" + distance + ", calories=" + calories
				+ ", floors=" + floors + ", mark=" + mark + ", user=" + user
				+ "]";
	}
}
