package au.usyd.hlr.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "HeartRate")
public class HeartRate implements Serializable {
	
	public HeartRate() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HeartRate(int logID, int restingheartrate, int normalheartrate,
			int aveheartrate, double mark, Date date, User user) {
		super();
		this.logID = logID;
		this.restingheartrate = restingheartrate;
		this.normalheartrate = normalheartrate;
		this.aveheartrate = aveheartrate;
		this.mark = mark;
		this.date = date;
		this.user = user;
	}

	
	@Id
	@GeneratedValue
	private int logID;

	@Column(name = "restingheartrate")
	private int restingheartrate;
	
	@Column(name = "normalheartrate")
	private int normalheartrate;
	
	@Column(name = "aveheartrate")
	private int aveheartrate;
	
	@Column(name = "mark")
	private double mark;
	
	@Column(name = "date")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="Userid")
	private User user;

	public int getLogID() {
		return logID;
	}

	public void setLogID(int logID) {
		this.logID = logID;
	}

	public int getRestingheartrate() {
		return restingheartrate;
	}

	public void setRestingheartrate(int restingheartrate) {
		this.restingheartrate = restingheartrate;
	}

	public int getNormalheartrate() {
		return normalheartrate;
	}

	public void setNormalheartrate(int normalheartrate) {
		this.normalheartrate = normalheartrate;
	}

	public int getAveheartrate() {
		return aveheartrate;
	}

	public void setAveheartrate(int aveheartrate) {
		this.aveheartrate = aveheartrate;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "HeartRate [logID=" + logID + ", restingheartrate="
				+ restingheartrate + ", normalheartrate=" + normalheartrate
				+ ", aveheartrate=" + aveheartrate + ", mark=" + mark
				+ ", date=" + date + ", user=" + user + "]";
	}

}
