package au.usyd.hlr.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Glucose")
public class Glucose implements Serializable{
	@Id
	@GeneratedValue
	private int logID;
	
	@Column(name = "mark")
	private double mark;
	
	@Column(name = "date")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="Userid")
	private User user;
	
	@Column(name = "avemoringglucose")
	private double avemoringglucose;
	
	@Column(name = "aveafternoonglucose")
	private double aveafternoonglucose;
	
	@Column(name = "aveeveningglucose")
	private double aveeveningglucose;

	@Column(name = "aveglucose")
	private double aveglucose;

	
	
	
	public Glucose() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Glucose(int logID, double mark, Date date, User user,
			double avemoringglucose, double aveafternoonglucose,
			double aveeveningglucose, double aveglucose) {
		super();
		this.logID = logID;
		this.mark = mark;
		this.date = date;
		this.user = user;
		this.avemoringglucose = avemoringglucose;
		this.aveafternoonglucose = aveafternoonglucose;
		this.aveeveningglucose = aveeveningglucose;
		this.aveglucose = aveglucose;
	}



	public int getLogID() {
		return logID;
	}

	public void setLogID(int logID) {
		this.logID = logID;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public double getAvemoringglucose() {
		return avemoringglucose;
	}

	public void setAvemoringglucose(double avemoringglucose) {
		this.avemoringglucose = avemoringglucose;
	}

	public double getAveafternoonglucose() {
		return aveafternoonglucose;
	}

	public void setAveafternoonglucose(double aveafternoonglucose) {
		this.aveafternoonglucose = aveafternoonglucose;
	}

	public double getAveeveningglucose() {
		return aveeveningglucose;
	}

	public void setAveeveningglucose(double aveeveningglucose) {
		this.aveeveningglucose = aveeveningglucose;
	}

	public double getAveglucose() {
		return aveglucose;
	}

	public void setAveglucose(double aveglucose) {
		this.aveglucose = aveglucose;
	}

	@Override
	public String toString() {
		return "Glucose [logID=" + logID + ", mark=" + mark + ", date=" + date
				+ ", user=" + user + ", avemoringglucose=" + avemoringglucose
				+ ", aveafternoonglucose=" + aveafternoonglucose
				+ ", aveeveningglucose=" + aveeveningglucose + ", aveglucose="
				+ aveglucose + "]";
	}
	
}
