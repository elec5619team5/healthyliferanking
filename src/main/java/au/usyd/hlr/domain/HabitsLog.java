package au.usyd.hlr.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="HabitsLog")
public class HabitsLog implements Serializable{
	
	@Id
	@GeneratedValue
	private int logID;
	
	@Column(name="StartDate")
	private String startDate;
	
	@Column(name="DrinkingDays")
	private int drinkingDays=0;
	
	@Column(name="SmokingDays")
	private int smokingDays=0;
	
	@Column(name="EatingDays")
	private int eatingWellDays=0;
	
	@Column(name = "Score")
	private int score;
	
	@ManyToOne
	@JoinColumn(name = "Username")
	private User user;
	
	public void updateScore(){
		int drinkingScore = 5;
		drinkingScore -= this.drinkingDays;
		
		int smokingScore = 5;
		smokingScore -= this.smokingDays;
		
		int eatingScore = -2;
		eatingScore += this.eatingWellDays;
		
		int newScore = drinkingScore + smokingScore + eatingScore;
		 
		this.setScore(newScore);
	}
	
	public int getLogID() {
		return logID;
	}
	public void setLogID(int logID) {
		this.logID = logID;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public int getDrinkingDays() {
		return drinkingDays;
	}
	public void setDrinkingDays(int drinkingDays) {
		this.drinkingDays = drinkingDays;
	}
	public int getSmokingDays() {
		return smokingDays;
	}
	public void setSmokingDays(int smokingDays) {
		this.smokingDays = smokingDays;
	}
	public int getEatingWellDays() {
		return eatingWellDays;
	}
	public void setEatingWellDays(int eatingWellDays) {
		this.eatingWellDays = eatingWellDays;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
}
