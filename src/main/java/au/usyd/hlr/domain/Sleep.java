package au.usyd.hlr.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Sleep")
public class Sleep implements Serializable {

	public Sleep() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Sleep(int logID, String timetobed, int timeinbed, int efficiency,
			double mark, Date date, User user) {
		super();
		this.logID = logID;
		this.timetobed = timetobed;
		this.timeinbed = timeinbed;
		this.efficiency = efficiency;
		this.mark = mark;
		this.date = date;
		this.user = user;
	}

	@Id
	@GeneratedValue
	private int logID;
	
	@Column(name = "timetobed")
	private String timetobed;
	
	@Column(name = "timeinbed")
	private int timeinbed;
	
	@Column(name = "efficiency")
	private int efficiency;
		
	@Column(name = "mark")
	private double mark;
	
	@Column(name = "date")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="Userid")
	private User user;

	public int getLogID() {
		return logID;
	}

	public void setLogID(int logID) {
		this.logID = logID;
	}

	public String getTimetobed() {
		return timetobed;
	}

	public void setTimetobed(String timetobed) {
		this.timetobed = timetobed;
	}

	public int getTimeinbed() {
		return timeinbed;
	}

	public void setTimeinbed(int timeinbed) {
		this.timeinbed = timeinbed;
	}

	public int getEfficiency() {
		return efficiency;
	}

	public void setEfficiency(int efficiency) {
		this.efficiency = efficiency;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Sleep [logID=" + logID + ", timetobed=" + timetobed
				+ ", timeinbed=" + timeinbed + ", efficiency=" + efficiency
				+ ", mark=" + mark + ", date=" + date + ", user=" + user + "]";
	}

}