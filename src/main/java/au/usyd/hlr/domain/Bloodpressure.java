package au.usyd.hlr.domain;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Bloodpressure")
public class Bloodpressure implements Serializable{
	
	@Id
	@GeneratedValue
	private int logID;
	
	@Column(name = "mark")
	private double mark;
	
	@Column(name = "date")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="Userid")
	private User user;
	
	@Column(name = "avebloodpressure")
	private double avebloodpressure;

	public int getLogID() {
		return logID;
	}

	public void setLogID(int logID) {
		this.logID = logID;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public double getAvebloodpressure() {
		return avebloodpressure;
	}

	public void setAvebloodpressure(double avebloodpressure) {
		this.avebloodpressure = avebloodpressure;
	}

	@Override
	public String toString() {
		return "Bloodpressure [logID=" + logID + ", mark=" + mark + ", date="
				+ date + ", user=" + user + ", avebloodpressure="
				+ avebloodpressure + "]";
	}

	public Bloodpressure() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Bloodpressure(int logID, double mark, Date date, User user,
			double avebloodpressure) {
		super();
		this.logID = logID;
		this.mark = mark;
		this.date = date;
		this.user = user;
		this.avebloodpressure = avebloodpressure;
	}

	
}
