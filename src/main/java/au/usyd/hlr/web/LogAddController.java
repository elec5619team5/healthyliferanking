package au.usyd.hlr.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.hlr.domain.BMILog;
import au.usyd.hlr.domain.HabitsLog;
import au.usyd.hlr.domain.User;
import au.usyd.hlr.service.BmiDbManager;
import au.usyd.hlr.service.HabitsDbManager;
import au.usyd.hlr.service.UserManager;

@Controller
public class LogAddController {

	protected final Log logger = LogFactory.getLog(getClass());

	@Resource(name = "BmiLogManager")
	private BmiDbManager bmiManager;

	@Resource(name = "HabitsLogManager")
	private HabitsDbManager habitsManager;

	@Resource(name = "DefaultUserManager")
	private UserManager userManager;

	@RequestMapping(value = "/addBmiLog", method = RequestMethod.POST)
	public String addBmiLog(HttpServletRequest httpServletRequest) {

		BMILog bmiLog = new BMILog();
		bmiLog.setStartDate(httpServletRequest.getParameter("startDate"));
		bmiLog.setHeight(Double.valueOf(httpServletRequest
				.getParameter("height")));
		bmiLog.setWeight(Integer.valueOf(httpServletRequest
				.getParameter("weight")));
		bmiLog.setBmiValue(bmiLog.calculateBmi());
		bmiLog.setCategory(bmiLog.determineBmiCategory());
		bmiLog.updateScore();
		
		// set user
		User user = this.userManager.getCurrentUser();
		bmiLog.setUser(user);

		this.bmiManager.addBmiLog(bmiLog);

		return "redirect:/bhl";
	}

	@RequestMapping(value = "/addHabitsLog", method = RequestMethod.POST)
	public String addHabitsLog(HttpServletRequest httpServletRequest) {

		HabitsLog habitsLog = new HabitsLog();
		
		int eatingDays = 0;
		if(httpServletRequest.getParameterValues("eating")!=null)
			eatingDays = httpServletRequest.getParameterValues("eating").length;
		
		int drinkingDays = 0 ;
		if(httpServletRequest.getParameterValues("drinking")!=null)
			drinkingDays = httpServletRequest.getParameterValues("drinking").length;
		
		int smokingDays = 0;
		if(httpServletRequest.getParameterValues("smoking")!=null)
			smokingDays = httpServletRequest.getParameterValues("smoking").length;
		
		habitsLog.setStartDate(httpServletRequest.getParameter("startDate"));
		habitsLog.setEatingWellDays(eatingDays);
		habitsLog.setDrinkingDays(drinkingDays);
		habitsLog.setSmokingDays(smokingDays);
		habitsLog.updateScore();

		// set user
		User user = this.userManager.getCurrentUser();
		habitsLog.setUser(user);

		this.habitsManager.addHabitsLog(habitsLog);

		return "redirect:/bhl";

	}

}
