package au.usyd.hlr.web;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.hlr.domain.User;
import au.usyd.hlr.service.FacebookService;
import au.usyd.hlr.service.UserManager;

@Controller
public class UserController {

  @RequestMapping(method = RequestMethod.GET, value = "/login")
  public String login() {
    return "login";
  }

  private static final Logger logger = LoggerFactory.getLogger(UserController.class);

  @Resource(name = "DefaultUserManager")
  private UserManager userManager;

  public void setUserManager(UserManager userManager) {
    this.userManager = userManager;
  }

  @RequestMapping(value = "/register", method = RequestMethod.GET)
  public String register(Model model) {
    logger.info("register");
    User user = new User();
    model.addAttribute(user);
    return "register";
  }


  @RequestMapping(value = "/register", method = RequestMethod.POST)
  public String register(@Valid User user, BindingResult result, Model model) {
    if (result.hasErrors()) {
      return "register";
    }

    if (user != null) {
      user.setAccountNonExpired(true);
      user.setAccountNonLocked(true);
      user.setCredentialsNonExpired(true);
      user.setEnabled(true);
      user.setRole(1);
      this.userManager.addUser(user);
    }

    return "login";
  }
  
  @RequestMapping(value="/profile", method = RequestMethod.GET)
  public String profile(Model model){
    User user = this.userManager.getCurrentUser();
    model.addAttribute(user);
    return "profile";
  }
  
  @RequestMapping(value="/profile", method = RequestMethod.POST)
  public String profile(User user){
    User currentUser = this.userManager.getCurrentUser();
    if(user.getInstitution() != null){
      currentUser.setInstitution(user.getInstitution());
    }
    if(user.getName() != null){
      currentUser.setName(user.getName());
    }
    if(user.getPassword() != null && !user.getPassword().isEmpty())
      currentUser.setPassword(user.getPassword());
    this.userManager.updateUser(currentUser);
    return "profile";
  }
}
