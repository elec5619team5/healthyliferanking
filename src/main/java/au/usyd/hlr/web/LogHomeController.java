package au.usyd.hlr.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.hlr.domain.BMILog;
import au.usyd.hlr.domain.HabitsCounter;
import au.usyd.hlr.domain.HabitsLog;
import au.usyd.hlr.service.BmiDbManager;
import au.usyd.hlr.service.HabitsDbManager;
import au.usyd.hlr.service.UserManager;


/**
 * Handles requests for the BMI and Habits Log home page.
 */

@Controller
public class LogHomeController{
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Resource(name = "BmiLogManager")
	private BmiDbManager bmiLogManager;
	
	@Resource(name = "HabitsLogManager")
	private HabitsDbManager habitsLogManager;
	
	@Resource(name = "DefaultUserManager")
	private UserManager userManager;

	@RequestMapping(value="/bhl")
	public ModelAndView handleRequest(HttpServletRequest arg0,
			HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		
		 Map<String,Object> myModel = new HashMap<String,Object>();
		 
		 List<BMILog> allBmiLog = this.bmiLogManager.getAllBmiLog(userManager.getCurrentUser());
		 List<HabitsLog> allHabitsLog = this.habitsLogManager.getAllHabitsLog(userManager.getCurrentUser());
		 
		 myModel.put("allBmiLog", allBmiLog);
		 myModel.put("allHabitsLog", allHabitsLog);
		 
		 //calculate total habits
		 HabitsCounter habitsCounter= new HabitsCounter(allHabitsLog);
		 habitsCounter.countTotal();
		 myModel.put("habitsCounter", habitsCounter);
		 
		 return new ModelAndView("log_home", "model", myModel);
	}
	
	@Autowired
	public void setBmiLogManager(BmiDbManager bmiLogManager){
		this.bmiLogManager=bmiLogManager;
	}

	@Autowired
	public void setHabitsLogManager(HabitsDbManager habitsLogManager) {
		this.habitsLogManager = habitsLogManager;
	}
	
	
	
}
