package au.usyd.hlr.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import au.usyd.hlr.domain.User;
import au.usyd.hlr.domain.steamModel;
import au.usyd.hlr.service.SteamManager;
import au.usyd.hlr.service.UserManager;

@Controller
public class steamController{
	protected final Log logger = LogFactory.getLog(getClass());
	String suggestion;
	
	@Resource(name = "steamManager")
	private SteamManager steamManager;
	
	@Resource(name = "DefaultUserManager")
	private UserManager userManager;
	
	//get value from form
	@RequestMapping(value = "/addSteamLog", method = RequestMethod.POST)
	public String addBmiLog(HttpServletRequest httpServletRequest) {
		
		if(httpServletRequest.getParameter("playingTime")==""||Integer.parseInt(httpServletRequest.getParameter("playingTime"))>24){
			return "steamForm";
		}
		
		steamModel aSteam=new steamModel();
		//set playing time
		aSteam.setPlayingTime(httpServletRequest.getParameter("playingTime"));
		//set score for today
		aSteam.setScore(calculateMark(httpServletRequest.getParameter("playingTime")));
		//set suggestions
		aSteam.setSuggestion(suggestion);
		
		// set user
		User user = this.userManager.getCurrentUser();
		aSteam.setUser(user);
		aSteam.setCustomerName(user.getUsername());
		//set record date
		Date date = new Date();
		aSteam.setRecordDate(date);
		
		//calculate total score
		List<steamModel> steamInfo;
		steamInfo=this.steamManager.getAllSteamLog(userManager.getCurrentUser());
		int totalScore=0;
		for(int i=0;i<steamInfo.size();i++){
			totalScore+=steamInfo.get(i).getScore();
		}
		totalScore+=aSteam.getScore();
		aSteam.setTotalscore(totalScore/(steamInfo.size()+1));

		this.steamManager.addSteam(aSteam);
		
		
		return "redirect:/updatesteam";
	}
	
	//return value to the view
	@RequestMapping(value="/updatesteam")
	public ModelAndView handleRequest(HttpServletRequest arg0,
			HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		 Map<String,Object> myModel = new HashMap<String,Object>();
		 myModel.put("PlayingTime", this.steamManager.getAllSteamLog(userManager.getCurrentUser()));
		 return new ModelAndView("steamView", "model", myModel);
	}
	
	//return statistics
	@RequestMapping(value="steamStatistics")
	public String steamStat(Model model){
		User user = this.userManager.getCurrentUser();
		
		List<steamModel> steamInfo;
		steamInfo=this.steamManager.getAllSteamLog(userManager.getCurrentUser());
		
		int score_0=0;
		int score_2=0;
		int score_3=0;
		int score_5=0;
		int score_10=0;
		int score_15=0;
		int score_20=0;
		int score_25=0;
		
		for(int i=0;i<steamInfo.size();i++){
			if(steamInfo.get(i).getScore()==0){
				score_0++;
			}else if(steamInfo.get(i).getScore()==2){
				score_2++;
			}else if(steamInfo.get(i).getScore()==3){
				score_3++;
			}else if(steamInfo.get(i).getScore()==5){
				score_5++;
			}else if(steamInfo.get(i).getScore()==10){
				score_10++;
			}else if(steamInfo.get(i).getScore()==15){
				score_15++;
			}else if(steamInfo.get(i).getScore()==20){
				score_20++;
			}else if(steamInfo.get(i).getScore()==25){
				score_25++;
			}
		}
		
		model.addAttribute("score_0", score_0);
		model.addAttribute("score_2", score_2);
		model.addAttribute("score_3", score_3);
		model.addAttribute("score_5", score_5);
		model.addAttribute("score_10", score_10);
		model.addAttribute("score_15", score_15);
		model.addAttribute("score_20", score_20);
		model.addAttribute("score_25", score_25);
		if(steamInfo.size()>0){
			model.addAttribute("totalscore", steamInfo.get(steamInfo.size()-1).getTotalscore());
		}else{
			model.addAttribute("totalscore", 0);
		}
		
		return "steamStatistic";
	}
	
	//calculate score and get suggestion
	public int calculateMark(String playingTime){
		int Score=0;
		
		if(Integer.parseInt(playingTime)<=3){
			Score=25;
			suggestion="Good lifeStyle in gaming.";
		}else if(Integer.parseInt(playingTime)>3 && Integer.parseInt(playingTime)<=4){
			Score=20;
			suggestion="Medium lifeStyle in gaming.";
		}
		else if(Integer.parseInt(playingTime)>4 && Integer.parseInt(playingTime)<=6){
			Score=15;
			suggestion="Spend a bit more time in gaming.";
		}
		else if(Integer.parseInt(playingTime)>6 && Integer.parseInt(playingTime)<=7){
			Score=10;
			suggestion="Spend too much time in gaming,and you need to reduce the gaming time to have a good lifestyle.";
		}
		else if(Integer.parseInt(playingTime)>7 && Integer.parseInt(playingTime)<=8){
			Score=5;
			suggestion="You need to control your gaming time.";
		}
		else if(Integer.parseInt(playingTime)>8 && Integer.parseInt(playingTime)<=9){
			Score=3;
			suggestion="Bad lifeStyle in gaming. You need to control your gaming time.";
		}
		else if(Integer.parseInt(playingTime)>9 && Integer.parseInt(playingTime)<=10){
			Score=2;
			suggestion="You are a game freak.";
		}else if(Integer.parseInt(playingTime)>10){
			Score=0;
			suggestion="You totally is a game freak.";
		}
		return Score;
	}
	
	@Autowired
	public void setSteamLogManager(SteamManager steamManager){
		this.steamManager=steamManager;
	}

}
