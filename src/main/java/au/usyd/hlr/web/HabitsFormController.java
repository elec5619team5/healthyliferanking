package au.usyd.hlr.web;

import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HabitsFormController {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@RequestMapping(value = "/habitsLog", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		
		logger.info("User visited Habits Log Form Page");
		
		return "habits_form";
	}

}
