package au.usyd.hlr.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.hlr.domain.HabitsCounter;
import au.usyd.hlr.domain.HabitsLog;
import au.usyd.hlr.service.HabitsDbManager;
import au.usyd.hlr.service.UserManager;


@Controller
public class LogChartsController {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Resource(name = "HabitsLogManager")
	private HabitsDbManager habitsLogManager;

	@Resource(name = "DefaultUserManager")
	private UserManager userManager;
	
	@RequestMapping(value="/logCharts")
	public ModelAndView handleRequest(HttpServletRequest arg0,
			HttpServletResponse arg1) throws Exception {
				
		Map<String,Object> myModel = new HashMap<String,Object>();
		List<HabitsLog> allHabitsLog = this.habitsLogManager.getAllHabitsLog(userManager.getCurrentUser());
		
		//calculate total habits
		HabitsCounter habitsCounter= new HabitsCounter(allHabitsLog);
		habitsCounter.countTotal();
		myModel.put("habitsCounter", habitsCounter);
		 
		return new ModelAndView("log_charts", "model", myModel);
		
	}
	
	@Autowired
	public void setHabitsLogManager(HabitsDbManager habitsLogManager) {
		this.habitsLogManager = habitsLogManager;
	}
	
}
