package au.usyd.hlr.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FitbitHomeController {

	@RequestMapping(value = "fitbithome")
	public String home() {
		return "fitbithome";
	}
}
