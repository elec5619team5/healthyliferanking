package au.usyd.hlr.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.usyd.hlr.domain.User;
import au.usyd.hlr.service.BmiDbManager;
import au.usyd.hlr.service.HabitsDbManager;
import au.usyd.hlr.service.Scorable;
import au.usyd.hlr.service.SteamManager;
import au.usyd.hlr.service.UserManager;
import au.usyd.hlr.util.Pair;

@Controller
public class DashBoardController {
  private static final Logger logger = LoggerFactory.getLogger(DashBoardController.class);

  @Resource(name = "DefaultUserManager")
  private UserManager userMananger;
  
  @Resource(name = "BmiLogManager")
  private Scorable bmiDbManager;
  
  @Resource(name = "HabitsLogManager")
  private Scorable habitsDbManager;

  @Resource(name = "steamManager")
  private Scorable steamManager;

  @Resource(name = "FacebookServiceValue")
  private Scorable facebookManager;

  @Resource(name = "DefaultFitbitManger")
  private Scorable fitbitManager;
  
  public void setUserMananger(UserManager userMananger) {
    this.userMananger = userMananger;
  }

  public void setBmiDbManager(Scorable bmiDbManager) {
    this.bmiDbManager = bmiDbManager;
  }

  public void setHabitsDbManager(Scorable habitsDbManager) {
    this.habitsDbManager = habitsDbManager;
  }

  public void setSteamManager(Scorable steamManager) {
    this.steamManager = steamManager;
  }

  public void setFacebookManager(Scorable facebookManager) {
    this.facebookManager = facebookManager;
  }

  public void setFitbitManager(Scorable fitbitManager) {
    this.fitbitManager = fitbitManager;
  }

  @RequestMapping(value = "/dashboard")
  public String home(Model model) {
    User user = this.userMananger.getCurrentUser();
    
    //generate leader board
    List<User> users = this.userMananger.getUsers();
    List<Pair<Integer, String>> leaderboard = new ArrayList<Pair<Integer, String>>();
    for (User u : users) {
      String name = u.getName();
      int score = getScore(u);
      if (name != null) {
        leaderboard.add(Pair.of(score, name));

      }
    }
    Collections.sort(leaderboard);
    logger.info(leaderboard.toString());
    model.addAttribute("leaderboard",leaderboard);

    //get personal score
    int bmiScore = this.habitsDbManager.getScore(user) + bmiDbManager.getScore(user);
    int facebookScore = this.facebookManager.getScore(user);
    int steamScore = this.steamManager.getScore(user);
    int fitbitScore = this.fitbitManager.getScore(user);

    //logger.info("bmi:" + Integer.toString(this.bmiDbManager.getScore(user)));
    //logger.info("habbit:" + Integer.toString(this.habitsDbManager.getScore(user)));

    model.addAttribute("bmiScore", bmiScore);
    model.addAttribute("steamScore", steamScore);
    model.addAttribute("facebookScore", facebookScore);
    model.addAttribute("fitbitScore", fitbitScore);

    return "dashboard";
  }

  public int getScore(User user) {
    int bmiScore = this.habitsDbManager.getScore(user) + bmiDbManager.getScore(user);
    int facebookScore = this.facebookManager.getScore(user);
    int steamScore = this.steamManager.getScore(user);
    int fitbitScore = this.fitbitManager.getScore(user);
    int total = bmiScore + facebookScore + steamScore + fitbitScore;
    return total;
  }
}
