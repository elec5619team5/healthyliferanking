package au.usyd.hlr.web;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.hlr.domain.FacebookData;
import au.usyd.hlr.service.FacebookService;

@Controller
@Scope("request")

public class FacebookController {
	private static final Logger logger = LoggerFactory.getLogger(FacebookController.class);
	
	@Resource(name="FacebookServiceValue")
	FacebookService facebookService;
	
	

	
	@RequestMapping(value="/facebook", method = RequestMethod.GET)
	public String facebook(HttpSession session, Model model)
	{
		String accessToken = (String)session.getAttribute("FacebookAccessToken");
		model.addAttribute("access", accessToken);
		List<FacebookData> checkFacebookData = facebookService.getFacebookDataList();
		if(accessToken!=null)
		{
			Date currentDate = new Date();
			facebookService.getUserProfile(accessToken);
			facebookService.getNumberOfStatuses(accessToken);
			facebookService.getNumberOfFriends(accessToken);
			facebookService.getNumberOfPhotos(accessToken);
			
			checkFacebookData = facebookService.getFacebookDataList();
			
			if(checkFacebookData.size()>0)
			{
				System.out.println(checkFacebookData.size());
				System.out.println("updating");
				if((currentDate.getTime()-facebookService.getLastestRecord().getRecordDate().getTime())/(24*60*60*1000)
						<=7)
				{
					facebookService.updateFacebookData();
				}
				else
				{
					facebookService.saveFacebookData(currentDate);
				}
			}
			else 
			{
				facebookService.saveFacebookData(currentDate);
			}
		
			
			FacebookData latestFacebookData = facebookService.getLastestRecord();
			System.out.println(latestFacebookData.getTotalScore());
			model.addAttribute("facebookData", latestFacebookData);			
			return "facebook";
		}
		else if(checkFacebookData.size()>0 && accessToken ==null)
		{
			FacebookData latestFacebookData = facebookService.getLastestRecord();
			model.addAttribute("facebookData", latestFacebookData);	
			return "facebook";
		}
		else
			return "facebook";
		
	}
	
	@RequestMapping(value="/facebookScore", method = RequestMethod.GET)
	public String facebookScoreDetail(Model model)
	{
		List<FacebookData> facebookDataList = facebookService.getFacebookDataList();
		if(facebookDataList.size()>=2)
		{
			FacebookData previousFacebookData = facebookDataList.get(facebookDataList.size()-1);
			model.addAttribute("preFacebookData", previousFacebookData);
			FacebookData latestFacebookData = facebookService.getLastestRecord();
			model.addAttribute("facebookData", latestFacebookData);
			return "facebookScoreDetail";
		}
		else if(facebookDataList.size()==1){
		
			FacebookData latestFacebookData = facebookService.getLastestRecord();
			model.addAttribute("facebookData", latestFacebookData);
			return "facebookScoreDetail";
		}
		else
			return "facebookScoreDetail";
		
		
		
	}
	
	@RequestMapping(value="/facebookProfile", method = RequestMethod.GET)
	public String facebookProfile(Model model)
	{
		List<FacebookData> facebookDataList = facebookService.getFacebookDataList();
		if(facebookDataList.size()>0)
		{
			FacebookData latestFacebookData = facebookService.getLastestRecord();
			model.addAttribute("facebookData", latestFacebookData);
			return "facebookProfile";
		}
		else
			return "facebookProfile";
		
			
		
		
	}
	
	@RequestMapping(value="/facebookHistory", method = RequestMethod.GET)
	public String facebookHistory(Model model)
	{
			List<FacebookData> facebookDataList = facebookService.getFacebookDataList();
			model.addAttribute("facebookDataList", facebookDataList);
			return "facebookHistory";
		
		
	}
	


	


}

