package au.usyd.hlr.web;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.LocalDate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.usyd.hlr.domain.User;
import au.usyd.hlr.service.FitbitManager;
import au.usyd.hlr.service.UserManager;

import com.fitbit.api.FitbitAPIException;
import com.fitbit.api.client.FitbitAPIEntityCache;
import com.fitbit.api.client.FitbitApiClientAgent;
import com.fitbit.api.client.FitbitApiCredentialsCache;
import com.fitbit.api.client.FitbitApiCredentialsCacheMapImpl;
import com.fitbit.api.client.FitbitApiEntityCacheMapImpl;
import com.fitbit.api.client.FitbitApiSubscriptionStorage;
import com.fitbit.api.client.FitbitApiSubscriptionStorageInMemoryImpl;
import com.fitbit.api.client.LocalUserDetail;
import com.fitbit.api.client.service.FitbitAPIClientService;
import com.fitbit.api.common.model.activities.Activities;
import com.fitbit.api.common.model.bp.BpLog;
import com.fitbit.api.common.model.glucose.GlucoseLog;
import com.fitbit.api.common.model.heart.HeartLog;
import com.fitbit.api.common.model.sleep.SleepLog;
import com.fitbit.api.common.model.user.UserInfo;
import com.fitbit.api.model.APIResourceCredentials;
import com.fitbit.api.model.FitbitUser;

@Controller
public class FitbitApiAuthController {

	public static final String OAUTH_TOKEN = "oauth_token";
	public static final String OAUTH_VERIFIER = "oauth_verifier";

	private FitbitAPIEntityCache entityCache = new FitbitApiEntityCacheMapImpl();
	private FitbitApiCredentialsCache credentialsCache = new FitbitApiCredentialsCacheMapImpl();
	private FitbitApiSubscriptionStorage subscriptionStore = new FitbitApiSubscriptionStorageInMemoryImpl();

	private static final String apiBaseUrl = "api.fitbit.com";
	private static final String fitbitSiteBaseUrl = "http://www.fitbit.com";
	private static final String exampleBaseUrl = "http://hlr.seewang.me/";
	private static final String clientConsumerKey = "fad008f9ff6c4d54962da9cba6e2d5a4";
	private static final String clientSecret = "7fda3833b9e0429ebf922d809da80d5e";

	@Resource(name = "DefaultUserManager")
	private UserManager userManager;
	@Resource(name = "DefaultFitbitManger")
	private FitbitManager fitbitManager;

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	@RequestMapping(value = "fitbitApiAuth")
	protected String doGet(HttpServletRequest request,
			HttpServletResponse response, Model model) throws IOException,
			ServletException {
		FitbitAPIClientService<FitbitApiClientAgent> apiClientService = new FitbitAPIClientService<FitbitApiClientAgent>(
				new FitbitApiClientAgent(apiBaseUrl, fitbitSiteBaseUrl,
						credentialsCache), clientConsumerKey, clientSecret,
				credentialsCache, entityCache, subscriptionStore);
		HttpSession session = request.getSession(true);
		session.setMaxInactiveInterval(20 * 60);

		if (request.getParameter("completeAuthorization") != null
				|| (session.getAttribute("tempReceived") != null && session
						.getAttribute("tempVerifier") != null)) {
			if ((session.getAttribute("tempReceived") != null && session
					.getAttribute("tempVerifier") != null)
					|| request.getParameter("completeAuthorization") != null) {
				String tempTokenReceived = new String();
				String tempTokenVerifier = new String();
				if ((session.getAttribute("tempReceived") == null && session
						.getAttribute("tempVerifier") == null)) {
					tempTokenReceived = new String(
							request.getParameter(OAUTH_TOKEN));
					tempTokenVerifier = (String) request
							.getParameter(OAUTH_VERIFIER);
				} else {
					tempTokenReceived = (String) session
							.getAttribute("tempReceived");
					tempTokenVerifier = (String) session
							.getAttribute("tempVerifier");
				}
				APIResourceCredentials resourceCredentials = apiClientService
						.getResourceCredentialsByTempToken(tempTokenReceived);

				if (resourceCredentials == null) {
					throw new ServletException(
							"Unrecognized temporary token when attempting to complete authorization: "
									+ tempTokenReceived);
				}
				// Get token credentials only if necessary:
				if (!resourceCredentials.isAuthorized()) {

					// The verifier is required in the request to get token
					// credentials:
					resourceCredentials.setTempTokenVerifier(tempTokenVerifier);
					try {

						// Get token credentials for user:
						apiClientService
								.getTokenCredentials(new LocalUserDetail(
										resourceCredentials.getLocalUserId()));
					} catch (FitbitAPIException e) {
						throw new ServletException(
								"Unable to finish authorization with Fitbit.",
								e);
					}
				}
				try {
					if (session.getAttribute("tempReceived") == null
							&& session.getAttribute("tempVerifier") == null) {
						UserInfo userInfo = apiClientService.getClient()
								.getUserInfo(
										new LocalUserDetail(resourceCredentials
												.getLocalUserId()));

						LocalDate today = new LocalDate().minusDays(1);

						Activities activities = apiClientService.getActivities(
								new LocalUserDetail(resourceCredentials
										.getLocalUserId()), today);
						double distances = 0;
						if (activities != null) {
							for (int i = 0; i < activities.getSummary()
									.getDistances().size(); i++) {
								distances = distances
										+ activities.getSummary()
												.getDistances().get(i)
												.getDistance();
							}
						}
						FitbitUser fitbituser = new FitbitUser(
								resourceCredentials.getLocalUserId());
						List<HeartLog> heartrate = apiClientService
								.getClient()
								.getLoggedHeartRate(
										new LocalUserDetail(
												resourceCredentials
														.getLocalUserId()),
										fitbituser, today).getHeartLog();
						List<BpLog> bplog = apiClientService
								.getClient()
								.getLoggedBp(
										new LocalUserDetail(
												resourceCredentials
														.getLocalUserId()),
										fitbituser, today).getBp();

						List<GlucoseLog> glucoselog = apiClientService
								.getClient()
								.getLoggedGlucose(
										new LocalUserDetail(
												resourceCredentials
														.getLocalUserId()),
										fitbituser, today).getGlucoseLog();
//						LocalDate lastday = new LocalDate("today");
						List<SleepLog> sleeplog = apiClientService
								.getClient()
								.getSleep(
										new LocalUserDetail(
												resourceCredentials
														.getLocalUserId()),
										fitbituser, today).getSleepLogs();

						double aveweekbp = 0;
						double aveweekmglucose = 0;
						double aveweekaglucose = 0;
						double aveweekeglucose = 0;
						double aveweekrheartrate = 0;
						double aveweeknheartrate = 0;
						double aveweekaheartrate = 0;
						for (int i = 0; i < 7; i++) {
							bplog = apiClientService
									.getClient()
									.getLoggedBp(
											new LocalUserDetail(
													resourceCredentials
															.getLocalUserId()),
											fitbituser, today.minusDays(i))
									.getBp();
							if (bplog != null && bplog.size() != 0) {
								aveweekbp = aveweekbp
										+ bplog.get(0).getDiastolic();
							}
							glucoselog = apiClientService
									.getClient()
									.getLoggedGlucose(
											new LocalUserDetail(
													resourceCredentials
															.getLocalUserId()),
											fitbituser, today.minusDays(i))
									.getGlucoseLog();
							if (glucoselog != null && glucoselog.size() != 0) {
								aveweekmglucose = aveweekmglucose
										+ glucoselog.get(0).getGlucose();
								aveweekaglucose = aveweekaglucose
										+ glucoselog.get(1).getGlucose();
								aveweekeglucose = aveweekeglucose
										+ glucoselog.get(2).getGlucose();
								System.out.println("aveweekmglucose: "
										+ aveweekmglucose);
								System.out.println("aveweekaglucose: "
										+ aveweekaglucose);
								System.out.println("aveweekeglucose: "
										+ aveweekeglucose);
							}
							heartrate = apiClientService
									.getClient()
									.getLoggedHeartRate(
											new LocalUserDetail(
													resourceCredentials
															.getLocalUserId()),
											fitbituser, today.minusDays(i))
									.getHeartLog();
							if (heartrate != null && heartrate.size() != 0) {
								System.out.println("in :" + i);
								aveweekrheartrate = aveweekrheartrate
										+ heartrate.get(0).getHeartRate();
								aveweeknheartrate = aveweeknheartrate
										+ heartrate.get(1).getHeartRate();
								aveweekaheartrate = aveweekaheartrate
										+ heartrate.get(2).getHeartRate();
							}
						}

						aveweekbp = aveweekbp / 7;
						aveweekmglucose = aveweekmglucose / 7;
						aveweekaglucose = aveweekaglucose / 7;
						aveweekeglucose = aveweekeglucose / 7;
						double aveweekglucose = (aveweekmglucose
								+ aveweekaglucose + aveweekeglucose) / 3;
						aveweekrheartrate = aveweekrheartrate / 7;
						aveweeknheartrate = aveweeknheartrate / 7;
						aveweekaheartrate = aveweekaheartrate / 7;
						System.out.println("aveweekmglucose: "
								+ aveweekmglucose);
						System.out.println("aveweekaglucose: "
								+ aveweekaglucose);
						System.out.println("aveweekeglucose: "
								+ aveweekeglucose);

						au.usyd.hlr.domain.Activities act = new au.usyd.hlr.domain.Activities();
						au.usyd.hlr.domain.Bloodpressure blo = new au.usyd.hlr.domain.Bloodpressure();
						au.usyd.hlr.domain.Glucose glu = new au.usyd.hlr.domain.Glucose();
						au.usyd.hlr.domain.HeartRate hea = new au.usyd.hlr.domain.HeartRate();
						au.usyd.hlr.domain.Sleep sle = new au.usyd.hlr.domain.Sleep();

						User user = this.userManager.getCurrentUser();
						Date newdate = new Date();
						act.setCalories(activities.getSummary()
								.getCaloriesOut());
						BigDecimal a = new BigDecimal((double) (distances));
						double anum = a.setScale(2, BigDecimal.ROUND_HALF_UP)
								.doubleValue();
						act.setDistance(anum);
						act.setSteps(activities.getSummary().getSteps());
						act.setUser(user);
						act.setDate(newdate);

						a = new BigDecimal(aveweekbp);
						aveweekbp = a.setScale(2, BigDecimal.ROUND_HALF_UP)
								.doubleValue();
						blo.setAvebloodpressure(aveweekbp);
						blo.setDate(newdate);
						blo.setUser(user);

						a = new BigDecimal(aveweekmglucose);
						aveweekmglucose = a.setScale(2,
								BigDecimal.ROUND_HALF_UP).doubleValue();
						glu.setAvemoringglucose(aveweekmglucose);
						a = new BigDecimal(aveweekaglucose);
						aveweekaglucose = a.setScale(2,
								BigDecimal.ROUND_HALF_UP).doubleValue();
						glu.setAveafternoonglucose(aveweekaglucose);
						a = new BigDecimal(aveweekeglucose);
						aveweekeglucose = a.setScale(2,
								BigDecimal.ROUND_HALF_UP).doubleValue();
						glu.setAveeveningglucose(aveweekeglucose);
						a = new BigDecimal(aveweekglucose);
						anum = a.setScale(2, BigDecimal.ROUND_HALF_UP)
								.doubleValue();
						glu.setAveglucose(anum);
						glu.setDate(newdate);
						glu.setUser(user);

						hea.setRestingheartrate((int) aveweekrheartrate);
						hea.setNormalheartrate((int) aveweeknheartrate);
						hea.setAveheartrate((int) aveweekaheartrate);
						hea.setDate(newdate);
						hea.setUser(user);

						if (sleeplog != null && sleeplog.size() >= 1) {
							DateFormat df = new SimpleDateFormat(
									"yyyy-MM-dd'T'HH:mm:ss.SSS");
							Date datebuffer = df.parse((String) sleeplog.get(0)
									.getStartTime());
							sle.setTimetobed(Integer.toString(datebuffer
									.getHours())
									+ ":"
									+ Integer.toString(datebuffer.getMinutes()));
							sle.setTimeinbed(sleeplog.get(0).getTimeInBed() / 60);
							sle.setEfficiency(sleeplog.get(0).getEfficiency());
							sle.setDate(newdate);
							sle.setUser(user);
						} else {
							sle.setTimetobed("00:00");
							sle.setTimeinbed(0);
							sle.setEfficiency(0);
							sle.setDate(newdate);
							sle.setUser(user);
						}

						fitbitManager.addFitbitData(act, blo, glu, hea, sle);
						session.setAttribute("userInfo", userInfo);
						request.setAttribute("userInfo", userInfo);
						;
						int totalscore = fitbitManager.getScore(user);
						model.addAttribute("totalscore", totalscore);
					} else {
						request.setAttribute("userInfo",
								session.getAttribute("userInfo"));
						User user = this.userManager.getCurrentUser();
						int totalscore = fitbitManager.getScore(user);
						model.addAttribute("totalscore", totalscore);

					}

				} catch (FitbitAPIException e) {
					throw new ServletException(
							"Exception during getting user info", e);
				} catch (Exception e) {
					System.out.println("Error: " + e);
				}
				session.setAttribute("tempReceived", (String) tempTokenReceived);
				session.setAttribute("tempVerifier", (String) tempTokenVerifier);
			}
			return "fitbithome";
		} else {
			System.out.println("CS");
			session.setAttribute("completeAuthorization", "notnull");
			try {
				response.sendRedirect(apiClientService
						.getResourceOwnerAuthorizationURL(new LocalUserDetail(
								"-"), exampleBaseUrl
								+ "/fitbitApiAuth?completeAuthorization="));
			} catch (FitbitAPIException e) {
				throw new ServletException(
						"Exception during performing authorization", e);
			}
		}
		return "fitbithome";
	}

	@RequestMapping(value = "FitbitMarks")
	public String showFitbitMakrs(Model model) {
		User user = this.userManager.getCurrentUser();
		au.usyd.hlr.domain.Activities newact = fitbitManager
				.getActivitiesByUser(user);
		au.usyd.hlr.domain.Bloodpressure newblo = fitbitManager
				.getBloodpressureByUser(user);
		au.usyd.hlr.domain.Glucose newglu = fitbitManager
				.getGlucoseByUser(user);
		au.usyd.hlr.domain.HeartRate newhea = fitbitManager
				.getHeartRateByUser(user);
		au.usyd.hlr.domain.Sleep newsle = fitbitManager.getSleepByUser(user);
		int totalscore = fitbitManager.getScore(user);

		model.addAttribute("activities", newact);
		model.addAttribute("bloodpressure", newblo);
		model.addAttribute("glucose", newglu);
		model.addAttribute("heartrate", newhea);
		model.addAttribute("sleep", newsle);
		model.addAttribute("totalscore", totalscore);
		return "fitbitmark";
	}

	@RequestMapping(value = "fitbitpersondetail")
	public String showPersonalDetail(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		HttpSession session = request.getSession(true);
		User user = this.userManager.getCurrentUser();

		request.setAttribute("userInfo", session.getAttribute("userInfo"));
		int totalscore = fitbitManager.getScore(user);
		model.addAttribute("totalscore", totalscore);
		return "fitbithome";
	}

	@RequestMapping(value = "Statics")
	public String showStatics(Model model) {
		User user = this.userManager.getCurrentUser();
		if (user != null) {
		
		au.usyd.hlr.domain.Activities newact = fitbitManager
				.getActivitiesByUser(user);
		au.usyd.hlr.domain.Bloodpressure newblo = fitbitManager
				.getBloodpressureByUser(user);
		au.usyd.hlr.domain.Glucose newglu = fitbitManager
				.getGlucoseByUser(user);
		au.usyd.hlr.domain.HeartRate newhea = fitbitManager
				.getHeartRateByUser(user);
		au.usyd.hlr.domain.Sleep newsle = fitbitManager.getSleepByUser(user);
		int totalscore = fitbitManager.getScore(user);

		model.addAttribute("activities", newact);
		model.addAttribute("bloodpressure", newblo);
		model.addAttribute("glucose", newglu);
		model.addAttribute("heartrate", newhea);
		model.addAttribute("sleep", newsle);
		model.addAttribute("totalscore", totalscore);
		}
		return "statics";
	}
}
