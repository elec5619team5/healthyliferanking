package au.usyd.hlr.web;

import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.hlr.domain.BMILog;
import au.usyd.hlr.service.BmiDbManager;
import au.usyd.hlr.service.UserManager;


/**
 * Handles requests for the BMI Log Form.
 */
@Controller
public class BmiFormController{
	
	@Resource(name = "DefaultUserManager")
	private UserManager userManager;
	
	@Resource(name = "BmiLogManager")
	private BmiDbManager bmiManager;
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@RequestMapping(value = "/bmiLog", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		
		logger.info("User visited BMI Log Form Page");
		
		BMILog lastBmiLog = bmiManager.getLatestLog(this.userManager.getCurrentUser());
		model.addAttribute("lastBmiLog", lastBmiLog);
		//model.addAttribute("lastHeight",lastBmiLog.getHeight());
		
		//model.addAttribute("test", "HELLO WORLD");
		
		return "bmi_form";
	}

}
