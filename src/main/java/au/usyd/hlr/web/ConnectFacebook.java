package au.usyd.hlr.web;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.usyd.hlr.domain.User;

@Controller
@RequestMapping(value = "/connect/**")
public class ConnectFacebook {

	private static final String SCOPE = "email,offline_access,user_about_me,user_friends,read_friendlists,user_status,user_photos";
	private static final String REDIRECT_URI = "http://hlr.seewang.me/connect/callback";
	private static final String CLIENT_ID = "306038479603603";
	private static final String APP_SECRET = "8e4f2138b4bc76669fb99f4250efc5b4";
	private static final String DIALOG_OAUTH = "https://www.facebook.com/dialog/oauth";
	private static final String ACCESS_TOKEN = "https://graph.facebook.com/oauth/access_token";

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public void connectFacebook(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			// TODO: if already have a valid access token, no need to redirect,
			// just login
			response.sendRedirect(DIALOG_OAUTH + "?client_id=" + CLIENT_ID
					+ "&redirect_uri=" + REDIRECT_URI + "&scope=" + SCOPE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/callback", method = RequestMethod.GET)
	public String getToken(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		String code = request.getParameter("code");
		if (code.isEmpty() == false) {
			String url = ACCESS_TOKEN + "?client_id=" + CLIENT_ID
					+ "&redirect_uri=" + REDIRECT_URI + "&client_secret="
					+ APP_SECRET + "&code=" + code;
			// Create an instance of HttpClient.
			try {
				HttpClient httpclient = new DefaultHttpClient();
				// Create a method instance.
				HttpGet httpget = new HttpGet(url);
				HttpResponse httpresponse = httpclient.execute(httpget);
				HttpEntity entity = httpresponse.getEntity();
				String responseStr = null;
				if (entity != null) {
					responseStr = EntityUtils.toString(entity);
				}

				if (responseStr != null) {
					String access_token[] = responseStr.split("&", 2);
					session.setAttribute("FacebookAccessToken", access_token[0]
							.replace("access_token=", "").trim());
					User user = new User();
					return "redirect:/facebook.htm";
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			return "redirect:/connect";
		}
		return "redirect:/connect";
	}

	@RequestMapping(value = "/callback", params = "error_reason", method = RequestMethod.GET)
	@ResponseBody
	public void error(@RequestParam("error_reason") String errorReason,
			@RequestParam("error") String error,
			@RequestParam("error_description") String description,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, description);
			System.out.println(errorReason);
			System.out.println(error);
			System.out.println(description);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
