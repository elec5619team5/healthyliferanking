package au.usyd.hlr.domain;

import org.junit.Test;

import junit.framework.TestCase;
import static org.junit.Assert.*;

public class FitbitSleepTest extends TestCase {
	private Sleep sleep;
	
	protected void setUp() {
		sleep = new Sleep();
	}
	@Test
	public void testGetAndSetTimetobed() {
		String timetobedBuffer = "10:30";
		sleep.setTimetobed(timetobedBuffer);
		assertEquals(timetobedBuffer, sleep.getTimetobed());
	}
	@Test
	public void testGetAndSetTimeinbed() {
		int timeinbedBuffer = 7;
		sleep.setTimeinbed(timeinbedBuffer);
		assertEquals(timeinbedBuffer, sleep.getTimeinbed());
	}
	@Test
	public void testGetAndSetEfficiency() {
		int efficiency = 80;
		sleep.setEfficiency(efficiency);
		assertEquals(efficiency, sleep.getEfficiency());
	}
	@Test
	public void testGetAndSetMark() {
		int mark = 3;
		sleep.setMark(mark);
		assertEquals(mark, (int)sleep.getMark());
	}
}
