package au.usyd.hlr.domain;

import java.util.ArrayList;
import java.util.Collection;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.springframework.security.core.GrantedAuthority;

public class UserTest extends TestCase{

  private User user;
  
  protected void setUp() throws Exception {
    user = new User();
  }
  
  public void testRole(){
    user.setRole(0);
    Collection<GrantedAuthority> userAuthority = (Collection<GrantedAuthority>) user.getAuthorities();
    user.setRole(1);
    Collection<GrantedAuthority> adminAuthority = (Collection<GrantedAuthority>) user.getAuthorities();
    assertNotSame(userAuthority, adminAuthority);
  }
  
}
