package au.usyd.hlr.domain;

import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class HabitsLogTest extends TestCase{
	
	private HabitsLog habitsLog;

	public void setUp() throws Exception {
		habitsLog = new HabitsLog();
	}

	@Test
	public void testUpdateScore() {
		
		int test_drinkingDays=0;
		int test_smokingDays=0;
		int test_eatingDays=0;
		int test_score=0;
		
		//case1
		test_drinkingDays=2;
		test_smokingDays=3;
		test_eatingDays=5;
		test_score=8;
		habitsLog.setDrinkingDays(test_drinkingDays);
		habitsLog.setSmokingDays(test_smokingDays);
		habitsLog.setEatingWellDays(test_eatingDays);
		habitsLog.updateScore();
		assertEquals(test_score,habitsLog.getScore());
		
		//case2
		test_drinkingDays=3;
		test_smokingDays=4;
		test_eatingDays=6;
		test_score=7;
		habitsLog.setDrinkingDays(test_drinkingDays);
		habitsLog.setSmokingDays(test_smokingDays);
		habitsLog.setEatingWellDays(test_eatingDays);
		habitsLog.updateScore();
		assertEquals(test_score,habitsLog.getScore());
		
		//case3
		test_drinkingDays=2;
		test_smokingDays=4;
		test_eatingDays=7;
		test_score=9;
		habitsLog.setDrinkingDays(test_drinkingDays);
		habitsLog.setSmokingDays(test_smokingDays);
		habitsLog.setEatingWellDays(test_eatingDays);
		habitsLog.updateScore();
		assertEquals(test_score,habitsLog.getScore());
		
		//fail("Not yet implemented");
	}

}
