package au.usyd.hlr.domain;

import junit.framework.TestCase;

import org.junit.Test;

import static org.junit.Assert.*;

public class FacebookDataTest extends TestCase{
	
	private FacebookData facebookData;
	
	protected void setUp() throws Exception {
		facebookData = new FacebookData();
	}
	
	@Test
	public void testFriendScoreCalculation() {
		
		//case 1: number of new friend is 0, friend score should be 0.0
		
		facebookData.setNewFriendNumber(0);
		assertEquals(facebookData.getFriendScore(),0.0);
		
		//case 2: number of new friend is greater than 0 and less than and equals 5, friend score should
		//be newFriendNumber * 1
		
		facebookData.setNewFriendNumber(2);
		assertEquals(facebookData.getFriendScore(),2.0);
		
		//case 3: number of new friend is greater than 5, friend score should be 5
				
		facebookData.setNewFriendNumber(6);
		assertEquals(facebookData.getFriendScore(),5.0);
		
		facebookData.setNewFriendNumber(8);
		assertEquals(facebookData.getFriendScore(),5.0);		
			
	
	}
	
	@Test
	public void testStatusScoreCalculation(){
		
		//case 1: number of new status is equal to 0, greater than 0, less than 20 and equal to 20
		//Status score should be number of new stuatus * 0.5
		facebookData.setNewStatusNumber(0);
		assertEquals(facebookData.getStatusScore(), 0.0);
		
		facebookData.setNewStatusNumber(10);
		assertEquals(facebookData.getStatusScore(), 5.0);
		
		facebookData.setNewStatusNumber(20);
		assertEquals(facebookData.getStatusScore(), 10.0);
		
		//case 2: number of new status is greater than 20
		//status score should be 10 - 0.5 * (number - 20)
		
		facebookData.setNewStatusNumber(21);
		assertEquals(facebookData.getStatusScore(), 9.5);
		
		facebookData.setNewStatusNumber(25);
		assertEquals(facebookData.getStatusScore(), 7.5);
		
		//case 3: number of new status is greater than 40, score should be 0.0
		
		facebookData.setNewStatusNumber(41);
		assertEquals(facebookData.getStatusScore(), 0.0);
		
		facebookData.setNewStatusNumber(45);
		assertEquals(facebookData.getStatusScore(), 0.0);
		
	}
	
	@Test
	public void testPhotoScoreCalculation(){
		
		//case 1: number of new photo is equal to 0, greater than 0, less than 20 and equal to 20
				//Status score should be number of new photo * 0.5
				facebookData.setNewPhotoNumber(0);
				assertEquals(facebookData.getPhotoScore(), 0.0);
				
				facebookData.setNewPhotoNumber(10);
				assertEquals(facebookData.getPhotoScore(), 5.0);
				
				facebookData.setNewPhotoNumber(20);
				assertEquals(facebookData.getPhotoScore(), 10.0);
				
		//case 2: number of new photo is greater than 20, score must be 10.0
				
				facebookData.setNewPhotoNumber(21);
				assertEquals(facebookData.getPhotoScore(), 10.0);
				
				facebookData.setNewPhotoNumber(40);
				assertEquals(facebookData.getPhotoScore(), 10.0);
				
				facebookData.setNewPhotoNumber(100);
				assertEquals(facebookData.getPhotoScore(), 10.0);
	}

}
