package au.usyd.hlr.domain;

import org.junit.Test;

import junit.framework.TestCase;
import static org.junit.Assert.*;

public class FitbitHeartrateTest extends TestCase {
	private HeartRate heartrate;
	
	protected void setUp() {
		heartrate = new HeartRate();
	}
	@Test
	public void testSetAndGetRestingHeartrate() {
		int restingheartrate = 70;
		heartrate.setRestingheartrate(restingheartrate);
		assertEquals(restingheartrate, heartrate.getRestingheartrate());
	}
	@Test
	public void testSetAndGetNormalHeartrate() {
		int normalheartrate = 70;
		heartrate.setNormalheartrate(normalheartrate);
		assertEquals(normalheartrate, heartrate.getNormalheartrate());
	}
	@Test
	public void testSetAndGetAveHeartrate() {
		int aveheartrate = 70;
		heartrate.setAveheartrate(aveheartrate);
		assertEquals(aveheartrate, heartrate.getAveheartrate());
	}
	@Test
	public void testSetAndGetMark() {
		int mark = 6;
		heartrate.setMark(mark);
		assertEquals(mark, (int)heartrate.getMark());
	}
}
