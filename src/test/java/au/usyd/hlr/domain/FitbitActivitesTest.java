package au.usyd.hlr.domain;

import junit.framework.TestCase;

import org.junit.Test;

public class FitbitActivitesTest extends TestCase {
	private Activities activities;
	
	protected void setUp() throws Exception {
		activities = new Activities();
	}
	@Test
	public void testSetAndGetSteps () {
		int steps = 10000;
		activities.setSteps(steps);
		assertEquals(steps, activities.getSteps());
	}
	@Test	
	public void testSetAndGetDistance() {
		double distance = 10000;
		activities.setDistance(distance);
		assertEquals(distance, activities.getDistance());
	}
	@Test
	public void testSetAndGetCalories() {
		double calories = 2500;
		activities.setCalories(calories);
		assertEquals(calories, activities.getCalories());
	}
	@Test
	public void testSetAndgetMark() {
		int mark = 5;
		activities.setMark(mark);
		assertEquals(mark, (int)activities.getMark());
	}

}
