package au.usyd.hlr.domain;

import org.junit.Test;

import junit.framework.TestCase;
import static org.junit.Assert.*;

public class FitbitBloodpressureTest extends TestCase{
	private Bloodpressure bloodpressure;
	
	protected void setUp() {
		bloodpressure = new Bloodpressure();
	}
	@Test
	public void testGetAndSetAveBloodpressure() {
		double avebloodpressure = 120;
		bloodpressure.setAvebloodpressure(avebloodpressure);
		assertEquals(avebloodpressure, bloodpressure.getAvebloodpressure());
	}
	@Test
	public void testGetAndSetMark() {
		int mark = 3;
		bloodpressure.setMark(mark);
		assertEquals(mark, (int)bloodpressure.getMark());
	}

}
