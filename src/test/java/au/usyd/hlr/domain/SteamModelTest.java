package au.usyd.hlr.domain;

import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.Test;

public class SteamModelTest extends TestCase {

	private steamModel steammodel;
	
	protected void setUp() throws Exception {
		steammodel = new steamModel();
	}
	
	@Test
	public void testGetPlayingTime(){
		String playingTime="12";
		steammodel.setPlayingTime("12");
		assertEquals(playingTime,steammodel.getPlayingTime());	
	}
	
	@Test
	public void testGetScore(){
		int Score=12;
		steammodel.setScore(12);
		assertEquals(Score,steammodel.getScore());	
	}
	
	@Test
	public void testGetName(){
		String Name="bob";
		steammodel.setCustomerName("bob");
		assertEquals(Name,steammodel.getCustomerName());	
	}
}
