package au.usyd.hlr.domain;

import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.Test;

public class BMILogTest extends TestCase{

	private BMILog bmiLog;
	
	protected void setUp() throws Exception {
		bmiLog = new BMILog();
	}
	
	@Test
	public void testCalculateBmi() {
		double test_height = 1.70;
		int test_weight = 65;
		bmiLog.setHeight(test_height);
		bmiLog.setWeight(test_weight);
		
		double test_bmi_value = test_weight / (test_height * test_height);
		assertEquals(bmiLog.calculateBmi(),test_bmi_value);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testDetermineBmiCategory() {
		
		double test_height = 0.0;
		int test_weight = 0;
		
		//case 1
		test_height = 1.70;
		test_weight = 65;
		bmiLog.setHeight(test_height);
		bmiLog.setWeight(test_weight);
		bmiLog.setBmiValue(bmiLog.calculateBmi());
		assertEquals("Normal",bmiLog.determineBmiCategory());
		
		//case 2
		test_height = 1.70;
		test_weight = 77;
		bmiLog.setHeight(test_height);
		bmiLog.setWeight(test_weight);
		bmiLog.setBmiValue(bmiLog.calculateBmi());
		assertEquals("Pre-obese",bmiLog.determineBmiCategory());
		
	}

	@Test
	public void testUpdateScore() {
		
		double test_height = 0.0;
		int test_weight = 0;
		
		//case 1
		test_height = 1.70;
		test_weight = 65;
		bmiLog.setHeight(test_height);
		bmiLog.setWeight(test_weight);
		bmiLog.setBmiValue(bmiLog.calculateBmi());
		bmiLog.updateScore();
		assertEquals(10,bmiLog.getScore());
		
		//case 1
		test_height = 1.70;
		test_weight =77;
		bmiLog.setHeight(test_height);
		bmiLog.setWeight(test_weight);
		bmiLog.setBmiValue(bmiLog.calculateBmi());
		bmiLog.updateScore();
		assertEquals(7,bmiLog.getScore());
		
	}

}
