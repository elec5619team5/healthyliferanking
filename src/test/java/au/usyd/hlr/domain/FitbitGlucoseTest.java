package au.usyd.hlr.domain;

import org.junit.Test;

import junit.framework.TestCase;
import static org.junit.Assert.*;

public class FitbitGlucoseTest extends TestCase {
	private Glucose glucose;
	
	protected void setUp() throws Exception {
		glucose = new Glucose();
	}
	@Test
	public void testSetAndGetAvemoringglucose () {
		double morningglucose = 120;
		glucose.setAvemoringglucose(morningglucose);
		assertEquals(morningglucose, glucose.getAvemoringglucose());
	}
	@Test
	public void testSetAndGetAveafternoonglucose() {
		double afternoonglucose = 120;
		glucose.setAveafternoonglucose(afternoonglucose);
		assertEquals(afternoonglucose, glucose.getAveafternoonglucose());
	}
	@Test
	public void testSetAndGetAveEveningglucose() {
		double eveningglucose = 120;
		glucose.setAveeveningglucose(eveningglucose);
		assertEquals(eveningglucose, glucose.getAveeveningglucose());
	}
	@Test
	public void testSetAndGetAveglucoseMark() {
		int mark = 5;
		glucose.setMark(mark);
		assertEquals(mark, (int)glucose.getMark());
	}
}
