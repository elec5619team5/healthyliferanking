package au.usyd.hlr.web;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

public class SteamFormTest {

	@InjectMocks
	private SteamForm steamform;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	 public void testSteamForm() {
		Locale locale= new Locale("test");
		Model model=new ExtendedModelMap();;
	    String result = this.steamform.home(locale, model);
	    assertEquals("steamView",result);
	  }

}
