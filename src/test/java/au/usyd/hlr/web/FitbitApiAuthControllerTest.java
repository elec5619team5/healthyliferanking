package au.usyd.hlr.web;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.usyd.hlr.domain.User;
import au.usyd.hlr.service.UserManager;

@RunWith(MockitoJUnitRunner.class)
public class FitbitApiAuthControllerTest {
  
  @Mock
  private UserManager userManager;
  
  
  @Mock
  private UserController userController;  
  
  
  @InjectMocks
  private FitbitApiAuthController fitbitapicontroller;
  
  Model model;
  
  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    model = new ExtendedModelMap();
    
  }
  
  @Test
  public void testShowStatics(){
	userController.profile(new User());
    String showstaticsResult = this.fitbitapicontroller.showStatics(new ExtendedModelMap());
    System.out.println(showstaticsResult);
    assertEquals("statics", showstaticsResult);
  }
  
//  @Test
//  public void testRegisterPost(){
//    User user = null;
//    Mockito.when(result.hasErrors()).thenReturn(true);
//    
//    String registerResult = this.userController.register(user, result, model);
//    assertEquals("register",registerResult);
//    
//    Mockito.when(result.hasErrors()).thenReturn(false);
//    registerResult = this.userController.register(user, result, model);
//    assertEquals("login",registerResult);
//  }
//  
//  @Test
//  public void testProfileGet(){
//    Mockito.when(userManager.getCurrentUser()).thenReturn(new User());
//    String profileResult = this.userController.profile(new ExtendedModelMap());
//    assertEquals("profile",profileResult);
//  }
//  
//  @Test
//  public void testProfilePost(){
//    User user = new User();
//    String profileResult = this.userController.profile(user);
//    assertEquals("profile",profileResult);
//  }
}
