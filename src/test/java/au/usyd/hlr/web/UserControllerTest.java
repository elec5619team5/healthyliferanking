package au.usyd.hlr.web;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import au.usyd.hlr.domain.User;
import au.usyd.hlr.service.UserManager;

public class UserControllerTest{
  
  @Mock
  private UserManager userManager;
  
  
  @Mock
  private BindingResult result;
  
  @InjectMocks
  private UserController userController;
  
  Model model;
  
  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    model = new ExtendedModelMap();
  }
  
  @Test
  public void testRegisterGet(){
    
    String registerResult = this.userController.register(new ExtendedModelMap());
    assertEquals("register",registerResult);
  }
  
  @Test
  public void testRegisterPost(){
    User user = null;
    Mockito.when(result.hasErrors()).thenReturn(true);
    
    String registerResult = this.userController.register(user, result, model);
    assertEquals("register",registerResult);
    
    Mockito.when(result.hasErrors()).thenReturn(false);
    registerResult = this.userController.register(user, result, model);
    assertEquals("login",registerResult);
  }
  
  @Test
  public void testProfileGet(){
    Mockito.when(userManager.getCurrentUser()).thenReturn(new User());
    String profileResult = this.userController.profile(new ExtendedModelMap());
    assertEquals("profile",profileResult);
  }
  
  @Test
  public void testProfilePost(){
    User user = new User();
    String profileResult = this.userController.profile(user);
    assertEquals("profile",profileResult);
  }
}
