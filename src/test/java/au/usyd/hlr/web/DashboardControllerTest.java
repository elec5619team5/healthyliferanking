package au.usyd.hlr.web;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.usyd.hlr.domain.User;
import au.usyd.hlr.service.Scorable;
import au.usyd.hlr.service.UserManager;

public class DashboardControllerTest{
  
  @Mock
  private UserManager userMananger;

  @Mock
  private Scorable scorable;
  
  @InjectMocks
  private DashBoardController dashBoardController;
  
  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    this.dashBoardController.setUserMananger(userMananger);
    this.dashBoardController.setBmiDbManager(scorable);
    this.dashBoardController.setFacebookManager(scorable);
    this.dashBoardController.setFitbitManager(scorable);
    this.dashBoardController.setHabitsDbManager(scorable);
    this.dashBoardController.setSteamManager(scorable);
  }
  
  @Test
  public void testHome() {
    Model model = new ExtendedModelMap();
    User user = new User();
    Mockito.when(scorable.getScore(user)).thenReturn(10);
    String result = this.dashBoardController.home(model);
    assertEquals("dashboard",result);
  }

}
