package au.usyd.hlr.web;

import static org.junit.Assert.assertEquals;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import au.usyd.hlr.domain.User;
import au.usyd.hlr.service.SteamManager;
import au.usyd.hlr.service.UserManager;


public class SteamStatTest {

	 @Mock
	  private UserManager userManager;
	 
	 @Mock
	 private SteamManager steamManager;
	 
	 @InjectMocks
	 private steamController steamController;
	 
	 Model model;

	 @Before
	  public void setup() {
	    MockitoAnnotations.initMocks(this);  
	    model = new ExtendedModelMap();
	  }
	 
	 @Test
	  public void testSteamStat(){
	    String profileResult = this.steamController.steamStat(new ExtendedModelMap());
	    assertEquals("steamStatistic",profileResult);
	  }
	 
	 
}
