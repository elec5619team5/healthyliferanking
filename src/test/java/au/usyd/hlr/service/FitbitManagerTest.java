package au.usyd.hlr.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import au.usyd.hlr.domain.Activities;
import au.usyd.hlr.domain.Bloodpressure;
import au.usyd.hlr.domain.Glucose;
import au.usyd.hlr.domain.HeartRate;
import au.usyd.hlr.domain.Sleep;
import au.usyd.hlr.domain.User;


public class FitbitManagerTest {
	private static FitbitMangerImpl mockedfitbitmanager;
	private static Activities act1;
	private static Activities act2;
	private static Glucose glu1;
	private static Glucose glu2;
	private static HeartRate hr1;
	private static HeartRate hr2;
	private static Bloodpressure bp1;
	private static Bloodpressure bp2;
	private static Sleep sle1;
	private static Sleep sle2;
	
	@BeforeClass
	public static void setUp() {
		mockedfitbitmanager = mock(FitbitMangerImpl.class);
		act1 = new Activities(1, 10000, 10000, 2000, 1000, 0, new Date(), new User());//5
		act2 = new Activities(1, 20000, 20000, 810, 100, 0, new Date(), new User());//1
		glu1 = new Glucose(1, 0, new Date(), new User(), 4, 4, 4, 4);//5
		glu2 = new Glucose(1, 0, new Date(), new User(), 5, 5, 5, 5);//4
		hr1 = new HeartRate(1, 70, 70, 70, 0, new Date(), new User());//5
		hr2 = new HeartRate(1, 81, 81, 81, 0, new Date(), new User());//1
		bp1 = new Bloodpressure( 1, 0, new Date(), new User(), 120);//5
		bp2 = new Bloodpressure( 1, 0, new Date(), new User(), 131);//1
		sle1 = new Sleep(1, "10:30", 7, 100, 0, new Date(), new User());//5
		sle2 = new Sleep(1, "10:30", 8, 100, 0, new Date(), new User());//5
	}
	
	@Test
	public void testAddFitbitData() {
		when((int)mockedfitbitmanager.addFitbitData(act1, bp1, glu1, hr1, sle1)).thenReturn(25);
		when((int)mockedfitbitmanager.addFitbitData(act2, bp2, glu2, hr2, sle2)).thenReturn(12);
	}
}
